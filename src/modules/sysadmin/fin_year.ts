import { api, cache } from '../../connectors'

export async function GetMeTheCurrentFinYearID(ctx: any): Promise<any> {
    const yearId = await cache.get('GetMeTheCurrentFinYearID')
    if (yearId) {
        return yearId
    }
    const currentDate = new Date()
    const variables = {
        currentDate: currentDate,
    }
    const query = `query admFinYears($currentDate:DateTime!){
      admFinYears(where:{
        start_date: { lte: $currentDate },
        end_date: { gte: $currentDate }
      }
      ){
        id
      }
    }`
    const result = await api.request(process.env.SYS_ADM_API_URL || '', query, variables, ctx.rawToken, ctx.token)
    if (result.admFinYears && result.admFinYears.length > 0) {
        cache.set('GetMeTheCurrentFinYearID', result.admFinYears[0].id)
        return result.admFinYears[0].id
    }
    return ''
}

// GetFinYearForSeqGeneration -To get the fin year for the reference sequence generation
export async function GetFinYearForSeqGeneration(ctx: any): Promise<string> {
    let yearID = ''
    yearID = await GetMeTheCurrentFinYearID(ctx)
    if (!yearID || yearID === '') {
        return yearID
    }

    const years = yearID.split('-')
    return years[0].substr(2, 2) + '-' + years[1].substr(2, 2)
}
