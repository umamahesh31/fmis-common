import { GetFinYearForSeqGeneration, GetMeTheCurrentFinYearID } from './fin_year'
import { GetCurrency } from './currency'

export const sysadmin = {
    GetFinYearForSeqGeneration,
    GetMeTheCurrentFinYearID,
    GetCurrency,
}
