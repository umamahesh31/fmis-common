import { api, cache } from '../../connectors'

export async function GetCurrency(ctx: any, currency: string): Promise<string> {
    await getSetting(ctx, currency)
    return cache.get(currency)
}

export async function getSetting(ctx: any, currency: string) {
    const query = `query appSetting($where:AppSettingWhereUniqueInput!){
  appSetting(where:$where){
  value1
  }
}`
    const result = await api.request(process.env.SYS_ADM_API_URL || '', query, { where: { id: currency } }, ctx.rawToken || '', ctx.token)
    const countryQuery = `query Countries($where:CountryWhereInput){
      countries(where:$where){
        ccy
      }
      }`
    const countryResult = await api.request(
        process.env.SYS_ADM_API_URL || '',
        countryQuery,
        { where: { ccy_code: { equals: result?.appSetting?.value1 } } },
        ctx.rawToken || '',
        ctx.token,
    )
    await cache.set(currency, countryResult?.countries[0]?.ccy)
}
