// import { error } from '@tectoro/connectors'

export const sanitiser = (req: any, res: any, next: any): void => {
    res.removeHeader('Transfer-Encoding')
    res.removeHeader('X-Powered-By')

    next()
}

// global error handler

export const errorHandler = async (req: any, res: any, next: any): Promise<void> => {
    try {
        await next()
    } catch (error) {
        console.log('Internal error')
        console.error(error)
        error.throwServiceError('system.global.error')
    }
}
