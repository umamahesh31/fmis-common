import { auth, api, cache, AuthToken } from '../connectors'
const SUPER_ADMIN = 'SUPER_ADMIN'
const CONCURRENT_APPROVER = 'CONCURRENT_APPROVER'
const GLOBAL_CIAU = 'GLOBAL_CIAU'

const getTenant = (token: AuthToken | undefined) => {
    if (token && token.tenantId) {
        return token.tenantId
    }

    return process.env.DEFAULT_TENANT || 'TEN1'
}

const readRawToken = (params: any): AuthToken | undefined => {
    params.res.set({ 'Access-Control-Expose-Headers': 'x-refresh-token,x-server-date' })
    params.res.setHeader('x-server-date', new Date().toISOString())
    if (params.req.headers.authorization && params.req.headers.authorization.split(' ')[0] === 'Bearer') {
        try {
            return auth.deencrypt(params.req.headers.authorization.split(' ')[1])
        } catch (err) {
            if (err.message === 'jwt expired') {
                const token = auth.refresh(params.req.headers.authorization.split(' ')[1])
                params.req.headers.authorization = 'Bearer ' + token
                params.res.setHeader('x-refresh-token', token)
                return readRawToken(params)
            }
            console.log('Failed to decrypt auth token', err)
        }
    }

    return undefined
}

const readFromHeaders = (req: any) => {
    return {
        email: req.headers['x-email'],
        tenantId: req.headers['x-tenantid'],
        userId: req.headers['x-userid'],
        username: req.headers['x-username'],
    } as AuthToken
}

const getToken = (params: any) => {
    if (params.req.headers['x-external'] === 'Y') {
        return readRawToken(params)
    } else {
        return readFromHeaders(params.req)
    }
}

const isAuthorized = async (username: string, functionCode: string, ...dataCodes: string[]) => {
    if (functionCode.endsWith('_C')) {
        if (await canHaveAccess(username, functionCode, ...dataCodes)) {
            return true
        } else {
            return await canHaveAccess(username, functionCode.slice(0, -1) + 'U', ...dataCodes)
        }
    } else {
        return await canHaveAccess(username, functionCode, ...dataCodes)
    }
}

const canHaveAccess = async (username: string, functionCode: string, ...dataCodes: string[]) => {
    const canAccessAll = await cache.exists(username + '#' + functionCode + '#' + 'ALL') // If the user has access for ALL, Only one entry will be there
    if (canAccessAll) {
        console.log(`${username + '#' + functionCode + '#' + 'ALL'} Can access all the keys.`)
        return true
    }
    if (dataCodes[0] === 'ALL') {
        // If the privilege is of GLOBAL and the role is of NON-GLOBAL, We should allow when the user has access for any
        const offices = await cache.get(username + '#' + functionCode + '#')
        if (offices) {
            console.log(`${username + '#' + functionCode + '#'} codes have the office code ALL`)
            return true
        }
    }
    for (const dataCode of dataCodes) {
        // Checking All office codes Access -If the list is multiple
        console.log('data code key:: ', dataCode)
        const dataCodeAccess = await cache.exists(username + '#' + functionCode + '#' + dataCode)
        console.log(`${username + '#' + functionCode + '#'} has the access on office code ${dataCode}`, dataCodeAccess)
        if (!dataCodeAccess) {
            return false
        }
    }
    return true
}

const dataCodes = async (username: string, functionCode: string) => {
    return await cache.get(username + '#' + functionCode + '#')
}

const applyDataCodes = async (username: string, functionCode: string, isInternal: boolean, args: any, ...dataFields: string[]) => {
    if (isInternal || (dataFields && dataFields[0] === 'ALL')) {
        return
    }
    if (process.env.INTERNAL_AUDIT && (await isCiauUser(username))) {
        return
    }

    const dataCodes = await cache.get_as_obj(username + '#' + functionCode + '#')
    args.where = args.where ?? {}
    if (!dataCodes) {
        args.where['id'] = { in: ['DUMMY_ID'] }
    } else if (dataCodes[0] !== 'ALL') {
        for (const dataField of dataFields) {
            args.where[dataField] = { in: dataCodes }
        }
    }
}

export const makeContext = (params: any): any => {
    let token = {} as AuthToken
    let isInternal = false
    if (!params.req) {
        return {
            token,
            isInternal: false,
            tenantId: getTenant(token as AuthToken),
            rawToken: '',
            isAuthorized,
            dataCodes,
            applyDataCodes,
            isSuperAdmin,
            isConcurrentApprover,
            isCiauUser,
        }
    }
    if (params.req.headers['x-internal'] === 'y') {
        isInternal = true
    }
    token = getToken(params) as AuthToken
    return {
        token,
        isInternal: isInternal,
        tenantId: getTenant(token),
        rawToken: params.req.headers.authorization,
        isAuthorized,
        dataCodes,
        applyDataCodes,
        isSuperAdmin,
        isConcurrentApprover,
        isCiauUser,
    }
}

export const isSuperAdmin = async (ctx: any): Promise<any> => {
    return await getUserRole(SUPER_ADMIN, ctx.token.username)
}

export const isConcurrentApprover = async (ctx: any): Promise<any> => {
    return await getUserRole(CONCURRENT_APPROVER, ctx.token.username)
}

export const isCiauUser = async (userName: string): Promise<any> => {
    return await getUserRole(GLOBAL_CIAU, userName)
}

async function getUserRole(roleId: string, userName: string): Promise<any> {
    let isValid = false
    if (!userName) {
        console.log('While validating the superadmin role, Unable to find the details of user name', userName)
        throw Error('Unable to find the details of user token')
    }
    const key = 'ADM_VALIDATION#' + userName + roleId
    const userRole = await cache.get(key)
    if (userRole) {
        return !isValid
    }

    const query = `
    query userRoles($role_id: String!, $user_name: String!, $is_active: Boolean!) {
        userRoles(
          where: {
            role_id: { equals: $role_id }
            user: { user_name: { equals: $user_name } }
            is_active: { equals: $is_active }
          }
        ) {
          id
          manager_id
          user_id
        }
      }
    `
    const result = await api.request(
        process.env.SYS_ADM_API_URL || '',
        query,
        { role_id: roleId, user_name: userName, is_active: true },
        '',
        {
            userId: 'SYSTEM',
            username: 'SYSTEM',
            email: '',
            tenantId: 'TEN1',
        },
    )
    if (result && result.userRoles && result.userRoles.length > 0) {
        await cache.set(key, result.userRoles)
        isValid = true
    }
    return isValid
}
