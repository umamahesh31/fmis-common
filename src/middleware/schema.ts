import { error } from '../connectors'

export const auth = (publicRoutes: string[]) => {
    return async (root: any, args: any, ctx: any, info: any, next: any): Promise<any> => {
        // don't process for non root resolvers
        if (info.path.prev) {
            return await next(root, args, ctx, info)
        }

        // authenticate
        // const resolver = `${_config.parentTypeConfig.name}.${_config.fieldConfig.name}`
        const resolver = `${info.parentType}.${info.path.key}`
        if (!publicRoutes.includes(resolver)) {
            if (!(ctx.token && ctx.token.userId)) {
                error.throwAuthError('system.auth.unauthorized')
            }
        }
        const result = await next(root, args, ctx, info)
        return result
    }
}

export const compute = () => {
    return async (root: any, args: any, ctx: any, info: any, next: any): Promise<any> => {
        // const resolver = `${_config.parentTypeConfig.name}.${_config.fieldConfig.name}`
        // const resolver = `${info.parentType}.${info.path.key}`
        if (args && args.data) {
            args.data.tenant_id = ctx.tenantId
            args.data['version_user'] = ctx.token.username
        }
        return await next(root, args, ctx, info)
    }
}
