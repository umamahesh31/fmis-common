/* eslint-disable camelcase */
// types
import { core } from 'nexus'

declare global {
    // Nexus Type extensions
    export type Arg<T extends string> = core.GetGen3<'argTypes', 'Mutation', T>
    export type MutationArg<T extends string> = Arg<T>
    export type QueryArg<T extends string> = core.GetGen3<'argTypes', 'Query', T>
}

// module
// export * from './nexus'

import { makeContext } from './context'
import * as server_middleware from './server'
import * as schema_middleware from './schema'

export const middleware = {
    makeContext,
    server_middleware,
    schema_middleware,
}
