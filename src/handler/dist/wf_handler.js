"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.initHandler = void 0;
var connectors_1 = require("@tectoro/connectors");
var client_1 = require("@prisma/client");
var dotenv_1 = require("dotenv");
exports.initHandler = function (clientId) { return __awaiter(void 0, void 0, void 0, function () {
    function getFunctionTableMap() {
        var map = new Map();
        map.set('SUPPLIER_CATEGORY', 'public."CntrSupplierCategory"');
        map.set('SUPPLIER_REGISTRATION', 'public."CntrSupplier"');
        map.set('CONTRACT_CREATION', 'public.CntrContract"');
        map.set('CONTRACT_BILL', 'public.ContractBill"');
        map.set('OTHER_BILL', 'public.OtherBill"');
        map.set('IMPREST_RELEASE', 'public.ImprestRelease"');
        map.set('IMPREST_ADJUSTMENT', 'public.ImprestAdjustment"');
        map.set('PAYMENT_INSTRUCTION', 'public.PaymentInstruction"');
        return map;
    }
    var db, handle, process1;
    return __generator(this, function (_a) {
        console.log('server:', process.env.NATS_STREAM_URL);
        dotenv_1.config();
        db = new client_1.PrismaClient();
        connectors_1.stream.subscribeWfUpdate(clientId, function (msg) { return __awaiter(void 0, void 0, void 0, function () {
            var seq;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('----------------------------------------');
                        seq = msg.mesg.getSequence().toString();
                        console.log(seq + "." + clientId + ". received request: " + msg.data);
                        return [4 /*yield*/, handle(seq, msg.data)];
                    case 1:
                        _a.sent();
                        console.log('----------------------------------------');
                        return [2 /*return*/];
                }
            });
        }); });
        handle = function (seq, input) { return __awaiter(void 0, void 0, void 0, function () {
            var err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, process1(input)];
                    case 1:
                        _a.sent();
                        console.log(seq + "." + clientId + ". finished");
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        console.error(seq + "." + clientId + ". Error: " + err_1.message);
                        console.log(seq + "." + clientId + ". failed");
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        process1 = function (input) { return __awaiter(void 0, void 0, void 0, function () {
            var functionTableMap, tableName, updateQry1, updateQry2, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        functionTableMap = getFunctionTableMap();
                        tableName = '';
                        tableName = functionTableMap.get(input["function"]) || '';
                        if (!(tableName != '')) return [3 /*break*/, 5];
                        updateQry1 = "UPDATE " + tableName + "  SET is_latest=FALSE, is_effective=FALSE WHERE ref='" + input.source_ref + "'";
                        if (input["function"] == 'SUPPLIER_CATEGORY') {
                            updateQry1 = "UPDATE " + tableName + "  SET is_latest=FALSE, is_effective=FALSE WHERE name='" + input.source_ref + "'";
                        }
                        updateQry2 = "UPDATE " + tableName + "  SET status='" + input.status + "', is_latest=TRUE, is_effective=is_active WHERE id='" + input.source_id + "'";
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, db.$executeRaw(updateQry1)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, db.$executeRaw(updateQry2)
                            // await db.$transaction([qry1, qry2])
                        ];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log('Error occurred while updating WF status', error_1);
                        console.log('Updates:', updateQry1, updateQry2);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        }); };
        return [2 /*return*/];
    });
}); };
