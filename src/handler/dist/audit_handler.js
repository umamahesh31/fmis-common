"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.publishAudit = void 0;
var connectors_1 = require("@tectoro/connectors");
var connectors_2 = require("@tectoro/connectors");
function publishAudit(wf_conf_id, type, record, action, rawToken, token, office_code) {
    return __awaiter(this, void 0, void 0, function () {
        var mutationObject, wfVariables;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    mutationObject = {
                        type: type,
                        action: action,
                        record: record
                    };
                    console.log('Audit Event Request Received:', mutationObject);
                    if (!(action === 'create' && wf_conf_id)) return [3 /*break*/, 2];
                    wfVariables = {
                        wf_conf_id: wf_conf_id,
                        source_id: record.id,
                        source_ref: record.ref,
                        source_version: record.version_no,
                        office_code: office_code || ''
                    };
                    return [4 /*yield*/, createWf(wfVariables, token, rawToken)];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2: return [4 /*yield*/, connectors_1.stream.publishMutation(mutationObject)];
                case 3:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
exports.publishAudit = publishAudit;
function createWf(variables, rawToken, token) {
    return __awaiter(this, void 0, void 0, function () {
        var mutation, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    mutation = "\n    mutation wfcreate($wf_conf_id:String!,$source_id:String!,$source_ref:String!,$prev_source_id:String,$source_version:Int!,$office_code:String!){\n        wfcreate(wf_conf_id:$wf_conf_id,source_id:$source_id,source_ref:$source_ref,prev_source_id:$prev_source_id,source_version:$source_version,office_code:$office_code)\n  }";
                    return [4 /*yield*/, connectors_2.api.request(process.env.EVENT_API_URL || '', mutation, variables, rawToken, token)];
                case 1:
                    result = _a.sent();
                    console.log('Workflow creation response:', result.wfcreate);
                    return [2 /*return*/, result.wfcreate];
            }
        });
    });
}
