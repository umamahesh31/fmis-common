import { api } from '../connectors'

export const generateDocuments = async (sourceId: string, documents: any, rawToken: string, token: any): Promise<void> => {
    if (!documents || !documents.data || documents.data.length === 0) {
        return
    }
    for (const doc of documents.data) {
        doc['source_id'] = sourceId
    }
    return await addDocuments(documents, rawToken, token)
}

async function addDocuments(documents: any, rawToken: string, token: any) {
    const mutation = `
    mutation createManyDocument($data: DocumentBulkCreateInput!) {
        createManyDocument(data: $data)
      }
    `
    const result = await api.request(process.env.SYS_ADM_API_URL || '', mutation, { data: { documents: documents.data } }, rawToken, token)
    console.log('Documents creation response:', result)
    return result
}

export const generateMultipleDocuments = async (sourceIdVsDocumentsMap: Map<string, any>, rawToken: string, token: any): Promise<void> => {
    const documentsData = []
    for (const [sourceId, documents] of sourceIdVsDocumentsMap) {
        if (!documents || !documents.data || documents.data.length === 0) {
            return
        }
        for (const doc of documents.data) {
            doc['source_id'] = sourceId
        }
        documentsData.push(documents.data)
    }

    return await addMultipleDocuments(documentsData, rawToken, token)
}

async function addMultipleDocuments(documents: any, rawToken: string, token: any) {
    const mutation = `
    mutation createManyDocument($data: DocumentBulkCreateInput!) {
        createManyDocument(data: $data)
      }
    `
    const result = await api.request(process.env.SYS_ADM_API_URL || '', mutation, { data: { documents: documents } }, rawToken, token)
    console.log('Documents creation response:', result)
    return result
}
