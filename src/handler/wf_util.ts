import { error, api } from '../connectors'

export const wfConfId = async (confId: string, officeId: string, ctx: any): Promise<any> => {
    const query = `
    query wfConfId($wf_conf_id: String!, $office_code: String!) {
        wfConfId(wf_conf_id: $wf_conf_id, office_code: $office_code)
      }
    `
    const result = await api.request(
        process.env.EVENT_API_URL || '',
        query,
        { wf_conf_id: confId, office_code: officeId },
        ctx.rawToken,
        ctx.token,
    )
    return result.wfConfId
}

export const validateNGetWfParams = (args: any): any => {
    // if (!args.data?.wf_params && !(args.data?.is_latest?.set || args.data?.status?.set === 'Approved')) { //TOFO NEED ADD BY IMPROVE
    //     error.throwUserError('Workflow Params are required to take action')
    // }
    if (!args.data?.wf_params) {
        return undefined
    }
    const wfParams = args.data?.wf_params
    delete args.data?.wf_params

    if (wfParams && !wfParams.fmis_function) {
        error.throwUserError('FMIS Function is required to take action')
    }
    if (wfParams && !wfParams.wf_conf_id) {
        error.throwUserError('Workflow ID is required to take action')
    }
    if (!wfParams.assign_to) {
        error.throwUserError('AssignTo is required to take action')
    }
    if (!wfParams.office_code) {
        error.throwUserError('Office Code is required to take action')
    }
    return wfParams
}

export const canSaveAllowed = async (ctx: any, functionCode: string, officeCode: string, wfParams: any): Promise<boolean> => {
    if (!(await ctx.isAuthorized(ctx.token.username, functionCode + '_U', officeCode))) {
        return false
    }
    return true
}
