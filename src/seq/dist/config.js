"use strict";
exports.__esModule = true;
exports.seqConfig = void 0;
// TODO: load this into cache
exports.seqConfig = {
    ADVANCE_REF: {
        type: 'ADVANCE_REF',
        key_template: 'AD_{0}',
        value_template: 'AD_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    BENEFICIARY_REF: {
        type: 'BENEFICIARY_REF',
        key_template: 'B_{0}',
        value_template: 'B_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    SUPPLIER_REF: {
        type: 'SUPPLIER_REF',
        key_template: 'S_{0}',
        value_template: 'S_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    CONTRACT_REF: {
        type: 'CONTRACT_REF',
        key_template: 'C_{0}',
        value_template: 'C_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    ADVANCE_BILL_REF: {
        type: 'ADVANCE_BILL_REF',
        key_template: 'AD_{0}',
        value_template: 'AD_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    RUNNING_BILL_REF: {
        type: 'RUNNING_BILL_REF',
        key_template: 'RB_{0}',
        value_template: 'RB_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    FINAL_BILL_REF: {
        type: 'FINAL_BILL_REF',
        key_template: 'FB_{0}',
        value_template: 'FB_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    SECURITY_DEPOSIT_REFUND_REF: {
        type: 'SECURITY_DEPOSIT_REFUND_REF',
        key_template: 'SDR_{0}',
        value_template: 'SDR_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    OTHER_BILL_REF: {
        type: 'OTHER_BILL_REF',
        key_template: 'OB_{0}',
        value_template: 'OB_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    IMPREST_RELEASE_REF: {
        type: 'IMPREST_RELEASE_REF',
        key_template: 'IR_{0}',
        value_template: 'IR_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    IMPREST_ADJUSTMENT_REF: {
        type: 'IMPREST_ADJUSTMENT_REF',
        key_template: 'IA_{0}',
        value_template: 'IA_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    CHALLAN_REF: {
        type: 'CHALLAN_REF',
        key_template: 'CH_{0}',
        value_template: 'CH_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    RECEIPT_REF: {
        type: 'RECEIPT_REF',
        key_template: 'RC_{0}',
        value_template: 'RC_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    },
    AADHAAR_REF: {
        type: 'AADHAAR_REF',
        key_template: '',
        value_template: '{seq}',
        input_fields: '',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 10
    },
    DEPOSIT_VOUCHER_REF: {
        type: 'DEPOSIT_VOUCHER_REF',
        key_template: 'DV_{0}',
        value_template: 'DV_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 7
    },
    PAYMENT_INSTRUCTION_REF: {
        type: 'PAYMENT_INSTRUCTION_REF',
        key_template: 'PI_{0}',
        value_template: 'PI_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 6
    },
    PAYMENT_VOUCHER_REF: {
        type: 'PAYMENT_VOUCHER_REF',
        key_template: 'PV_{0}',
        value_template: 'PV_{0}_{seq}',
        input_fields: 'bill_ref',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 2
    },
    PAYEE_TXN_REF: {
        type: 'PAYEE_TXN_REF',
        key_template: 'PT_{0}',
        value_template: 'PT_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 7
    },
    BANK_ACC_REF: {
        type: 'BANK_ACC_REF',
        key_template: 'B_{0}',
        value_template: 'B_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    CASH_ACC_REF: {
        type: 'CASH_ACC_REF',
        key_template: 'C_{0}',
        value_template: 'C_{0}_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    CHEQUE_REF: {
        type: 'CHEQUE_REF',
        key_template: '',
        value_template: '{seq}',
        input_fields: '',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 6
    },
    USER_NAME_REF: {
        type: 'USER_NAME_REF',
        key_template: '{0}',
        value_template: '{0}_{seq}',
        input_fields: 'first_name',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 3
    },
    ROLE_REF: {
        type: 'ROLE_REF',
        key_template: '',
        value_template: '{seq}',
        input_fields: '',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WORKFLOW_REF: {
        type: 'WORKFLOW_REF',
        key_template: 'WF',
        value_template: 'WF_{seq}',
        input_fields: '',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_SUPPLIER_CATEGORY_REF: {
        type: 'WF_SUPPLIER_CATEGORY_REF',
        key_template: 'TX_{0}_SC',
        value_template: 'TX_{0}_SC_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_SUPPLIER_REGISTRATION_REF: {
        type: 'WF_SUPPLIER_REGISTRATION_REF',
        key_template: 'TX_{0}_SR',
        value_template: 'TX_{0}_SR_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_CONTRACT_CREATION_REF: {
        type: 'WF_CONTRACT_CREATION_REF',
        key_template: 'TX_{0}_CM',
        value_template: 'TX_{0}_CM_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_ADVANCE_BILL_REF: {
        type: 'WF_ADVANCE_BILL_REF',
        key_template: 'TX_{0}_AD',
        value_template: 'TX_{0}_AD_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_RUNNING_BILL_REF: {
        type: 'WF_RUNNING_BILL_REF',
        key_template: 'TX_{0}_RB',
        value_template: 'TX_{0}_RB_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_FINAL_BILL_REF: {
        type: 'WF_FINAL_BILL_REF',
        key_template: 'TX_{0}_FB',
        value_template: 'TX_{0}_FB_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_SEC_DEPOSIT_REFUND_REF: {
        type: 'WF_SEC_DEPOSIT_REFUND_REF',
        key_template: 'TX_{0}_SDR',
        value_template: 'TX_{0}_SDR_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_OTHER_BILL_REF: {
        type: 'WF_OTHER_BILL_REF',
        key_template: 'TX_{0}_OB',
        value_template: 'TX_{0}_OB_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_IMPREST_RELEASE_REF: {
        type: 'WF_IMPREST_RELEASE_REF',
        key_template: 'TX_{0}_IR',
        value_template: 'TX_{0}_IR_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_IMPREST_ADJUSTMENT_REF: {
        type: 'WF_IMPREST_ADJUSTMENT_REF',
        key_template: 'TX_{0}_IA',
        value_template: 'TX_{0}_IA_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 4
    },
    WF_PAYMENT_INSTRUCTION_REF: {
        type: 'WF_PAYMENT_INSTRUCTION_REF',
        key_template: 'TX_{0}_PI',
        value_template: 'TX_{0}_PI_{seq}',
        input_fields: 'fin_year',
        dynamic_fields: '',
        start_value: 1,
        seq_size: 5
    }
};
