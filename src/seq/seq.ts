import { error } from '../connectors'
import { seqConfig, workflowConfig } from './config'

export const CONFIG = {
    get: (type: string): any => {
        // TODO: return this from cache
        return seqConfig[type]
    },
}

const format = function (template: string, ...params: any[]) {
    return template.replace(/{(\d+)}/g, function (match, number) {
        return typeof params[number] !== 'undefined' ? params[number] : match
    })
}

const applyInputFields = (keyTemplate: string, valueTemplate: string, input: Record<string, any>, inputFields: string) => {
    if (inputFields === '') {
        return [keyTemplate, valueTemplate]
    }

    const fields = inputFields.split(',')
    const values = []
    for (const field of fields) {
        if (!input[field]) {
            error.throwUserError('seq.key.missing', field)
        }
        values.push(input[field])
    }

    const formattedKey = format(keyTemplate, ...values)
    const formattedValue = format(valueTemplate, ...values)
    return [formattedKey, formattedValue]
}

const applyDynamicFields = (keyTemplate: string, valueTemplate: string, dynamicFields: string) => {
    if (dynamicFields === '') {
        return [keyTemplate, valueTemplate]
    }

    const fields = dynamicFields.split(',')
    for (const field of fields) {
        let value = ''
        if (field === 'YYYY') {
            value = new Date().getFullYear().toString()
        }
        // TODO: add the remaining dynamic fields

        keyTemplate = keyTemplate.replace('{' + field + '}', value)
        valueTemplate = valueTemplate.replace('{' + field + '}', value)
    }

    return [keyTemplate, valueTemplate]
}

export const generate = async (type: string, record: Record<string, any>, ctx: any): Promise<any> => {
    const config = CONFIG.get(type)

    if (!config) {
        error.throwServiceError('seq.config.missing', type)
    }
    // input fields
    let [key, value] = applyInputFields(config.key_template, config.value_template, record, config.input_fields)

    // dynamic fields
    ;[key, value] = applyDynamicFields(key, value, config.dynamic_fields)

    // seq field
    const seq = await ctx.db.seqCode.findUnique({
        where: { type_key: { type: type, key: key } },
    })
    // It invoke only for custom alpha seq
    if (config.value_template.includes('alpha_seq')) {
        return await generateAlphaSeq(seq, config, key, value, ctx)
    }
    const seqValue = seq ? seq.next_value : config.start_value
    value = value.replace('{seq}', padLeft(seqValue.toString(), config.seq_size))

    await ctx.db.seqCode.upsert({
        create: {
            type: config.type,
            key: key,
            next_value: seqValue + 1,
        },
        update: {
            next_value: seqValue + 1,
        },
        where: { type_key: { type: config.type, key: key } },
    })

    return value
}

function padLeft(str: any, size: number): any {
    str = '00000000000000000' + str.toString()
    return str.substr(-size)
}

export const wfSeq = (seqId: string): any => {
    return workflowConfig[seqId] || { function_name: 'unknown for the seq:' + seqId }
}

export const generateAlphaSeq = async (seq: any, config: any, key: string, value: string, ctx: any): Promise<any> => {
    let seqValue = seq ? seq.next_value : config.start_value
    let alphaSeq = seq ? seq.alpha_seq : config.value_template_details.alpha_seq_start_value
    let seqFormat: string = config.value_template_details.alpha_seq_format
    if (isMaxSeq(seqValue, config.seq_size)) {
        alphaSeq = nextAlphaSeq(alphaSeq)
        seqValue = config.start_value
    }
    seqFormat = seqFormat.replace('{alpha_seq}', alphaSeq).replace('{seq}', padLeft(seqValue.toString(), config.seq_size))
    value = value.replace('{alpha_seq}', seqFormat)
    await ctx.db.seqCode.upsert({
        create: {
            type: config.type,
            key: key,
            next_value: seqValue + 1,
            alpha_seq: alphaSeq,
        },
        update: {
            next_value: seqValue + 1,
            alpha_seq: alphaSeq,
        },
        where: { type_key: { type: config.type, key: key } },
    })

    return value
}

function nextAlphaSeq(seq: string) {
    const zCode = 90
    const aCode = 65
    let peek = false
    for (let index = seq.length - 1; index >= 0; index--) {
        if (seq.charCodeAt(index) === zCode) {
            peek = true
            seq = setCharAt(seq, index, String.fromCharCode(aCode))
            continue
        } else if (peek === true || index === seq.length - 1) {
            seq = setCharAt(seq, index, String.fromCharCode(seq.charCodeAt(index) + 1))
            break
        }
    }
    return seq
}

function setCharAt(str: string, index: number, chr: string) {
    if (index > str.length - 1) return str
    return str.substring(0, index) + chr + str.substring(index + 1)
}

function isMaxSeq(seqValue: number, size: number) {
    const target = parseInt('99999999999999999'.substring(0, size))
    return seqValue === target + 1
}
