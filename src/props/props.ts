import * as vaultLib from 'node-vault'
import { util } from '../connectors'
util.loadEnv()

export const loadProperties = async () => {
    try {
        const vault = vaultLib.default({
            apiVersion: process.env.VAULT_API_VERSION || 'v1',
            endpoint: process.env.VAULT_URL,
            token: process.env.VAULT_TOKEN,
        })
        console.log('config path',vault)
        console.log(process.env.CONFIG_PATH,'----------', process.env.VAULT_URL)
        const result = {
        "APP_NAME": "ia",
        "APP_SECRET": "MQgPg0T9wfr",
        "APP_URL": "http://10.89.166.166:30014/",
        "CHOKIDAR_USEPOLLING": true,
        "CLIENT_NAME": "Internal Audit",
        "CONFIG_PATH": "kv/pwc-ia",
        "CONTRACT_API_URL": "http://contract-api:4000/graphql",
        "CONTRACT_DB_URL": "postgresql://postgres:Mv53ANVcYsFs@10.89.166.166:5432/contract_ia",
        "COPY_RIGHTS": "| 2021 | PWC | All rights reserved |",
        "DB_TYPE": "postgres",
        "EMPLOYEE_API_URL": "http://employee-api:4000/graphql",
        "EMPLOYEE_DB_URL": "postgresql://postgres:Mv53ANVcYsFs@10.89.166.166:5432/employee_ia",
        "ENC_KEY": "d0a02ea09db63fe05e5faeb248a75519c1ac0cab24372e48b63779fd66a08bdf",
        "ENCRYPTION_ALGO": "aes-256-cbc",
        "EVENT_API_URL": "http://events-api:4000/graphql",
        "EVENT_DB_URL": "postgresql://postgres:Mv53ANVcYsFs@10.89.166.166:5432/event_ia",
        "EXPENDITURE_API_URL": "http://expenditure-api:4000/graphql",
        "GOTENBERG_URL": "http://pdf-gen:3000",
        "IA_API_URL": "http://ia-api:4000/graphql",
        "IA_DB_URL": "postgresql://postgres:Mv53ANVcYsFs@10.89.166.166:5432/ia",
        "INTERNAL_AUDIT": true,
        "JWT_SECRET": "MQgPg0T9wfr",
        "KEY_IV": "5541e32f6baf3328bf03f73d0490e99e",
        "MINIO_ACCESS_KEY": "minio",
        "MINIO_BUCKET": "ciauo",
        "MINIO_ENDPOINT": "10.89.166.166",
        "MINIO_PORT": 9000,
        "MINIO_SECRET_KEY": "minio123",
        "NATS_STREAM_CLUSTER": "test-cluster",
        "NATS_STREAM_URL": "nats://10.89.166.166:4223",
        "NATS_TOPIC": "example",
        "NATS_URL": "nats://10.89.166.166:4222",
        "REDIS_HOST": "10.89.166.166",
        "REDIS_PORT": 6379,
        "SMTP_FROM": "support-dev@tectoro.com",
        "SMTP_HOST": "email-smtp.eu-west-1.amazonaws.com",
        "SMTP_PASSWORD": "BNkxiqyzZeYkigQ0Fhy7NSH1SMYNSPOXQYQ1iUvksM9w",
        "SMTP_PORT": 465,
        "SMTP_SECURE": true,
        "SMTP_USER": "AKIA5STOTOC56QBUBXWK",
        "SUPPORT_EMAIL_ID": "ia-uat@tectoro.com",
        "SYS_ADM_API_URL": "http://sysadmin-api:4000/graphql",
        "SYS_ADMIN_DB_URL": "postgresql://postgres:Mv53ANVcYsFs@10.89.166.166:5432/sysadmin_ia",
        "VAULT_TOKEN": "",
        "VAULT_URL": "",
        "VERSION": "v10",
        }


        for (const entry of Object.entries<any>(result)) {
            process.env[entry[0]] = entry[1]
        }
    } catch (err) {
        console.log('in error', err)
        console.log('failed load to load properties from vault. exiting')
        process.exit()
    }
}
