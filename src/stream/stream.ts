import { Message } from 'node-nats-streaming'
import { stream } from '../connectors'
import { schema } from './schema'
import { BILL_EVENT } from './types'

export const CONFIG = {
    topics: {
        ...stream.CONFIG.topics,
    },
}

/* *************************************************
 Bill Payment streams
****************************************************/

export const subscribeBill = async (clientId: string, groupId: string, next: (msg: { mesg: Message; data: BILL_EVENT }) => void) => {
    return await stream.subscribe<BILL_EVENT>(clientId, groupId, CONFIG.topics.bill, next)
}

export const publishBill = async (input: BILL_EVENT) => {
    const errors = await schema.validate(input, schema.types.payment.bill)
    if (errors.length > 0) {
        return errors
    }

    await stream.publish(CONFIG.topics.bill, input)
    return []
}
