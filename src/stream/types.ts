/************************************************* */
// Bill Payment Event types
/************************************************* */
export type BILL_EVENT = {
  source_id: string
  version_no: number
  prev_version_no?: number
}
