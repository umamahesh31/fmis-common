import { connect, StartPosition, Message, Stan } from 'node-nats-streaming'
import { schema } from './schema'
import { MUTATION_EVENT, WF_STATUS_EVENT, AUDIT_EVENT, UPDATE_EVENT_HANDLER } from './types'

export function getVersion() {
    // <application_name>.<version>.<topic_name>
    return (process.env.APP_NAME || 'APP') + '_' + (process.env.VERSION || 'V10') + '_'
}
export const CONFIG = {
    topics: {
        mutation: 'mutation',
        wfstatus: 'wfstatus',
        audit: 'audit',
        bill: 'bill',
        update_event: 'update_event',
    },
}

export const doconnect = async (clientId: string): Promise<Stan> => {
    return new Promise(function (resolve, reject) {
        clientId = getVersion() + clientId
        const conn = connect(process.env.NATS_STREAM_CLUSTER || 'test-cluster', clientId, {
            url: process.env.NATS_STREAM_URL || 'nats://localhost:4223',
            reconnect: true,
        })
        conn.on('connect', async () => {
            resolve(conn)
        })
        conn.on('error', async (err) => {
            console.log('error occurred in connect:', err)
            reject(err)
        })
    })
}

export const subscribe = async <T extends unknown>(
    clientId: string,
    groupId: string,
    topic: string,
    next: (msg: { mesg: Message; data: T }) => void,
) => {
    const client = await doconnect(clientId)
    topic = getVersion() + topic
    const options = client.subscriptionOptions().setStartAt(StartPosition.NEW_ONLY).setDurableName(topic).setManualAckMode(true)
    // .setAckWait(1 * 60 * 1000) //1 min

    let subscription: any
    if (groupId !== '') {
        subscription = client.subscribe(topic, groupId, options)
        console.log(`connected to subject ${topic} on queue ${groupId} as ${clientId}`)
    } else {
        subscription = client.subscribe(topic, options)
        console.log(`connected to subject ${topic} as ${clientId}`)
    }

    subscription.on('message', async (mesg: Message) => {
        let body = {}
        try {
            body = JSON.parse(mesg.getData() as string)
        } catch (e) {
            console.log(e)
        }
        next({
            mesg,
            data: body as T,
        })
    })
    client.on('close', () => {
        console.log('closing subscription')
        subscription.unsubscribe()
    })
    process.on('exit', function (code) {
        subscription.unsubscribe()
        return console.log(`About to exit with code ${code}, Unsubscribing Subscripton with clientID: ${clientId}`)
    })
}

// TODO: maintain persistent connection
export const publish = async (topic: string, input: any): Promise<any> => {
    console.log('publishing the message')
    topic = getVersion() + topic
    const clientId = process.env.NATS_STREAM_CLIENTID || 'testpub'
    const conn = await doconnect(clientId)
    const resp = conn.publish(topic, JSON.stringify(input))
    conn.close()
    return resp
}

/** *********************************************** */
// Wrapper Functions
/** *********************************************** */

export const subscribeMutation = async (
    clientId: string,
    groupId: string,
    next: (msg: { mesg: Message; data: MUTATION_EVENT }) => void,
) => {
    return subscribe<MUTATION_EVENT>(clientId, groupId, CONFIG.topics.mutation, next)
}

export const subscribeAudit = async (clientId: string, groupId: string, next: (msg: { mesg: Message; data: AUDIT_EVENT }) => void) => {
    return subscribe<AUDIT_EVENT>(clientId, groupId, CONFIG.topics.audit, next)
}

export const subscribeWfUpdate = async (
    clientId: string,
    groupId: string,
    next: (msg: { mesg: Message; data: WF_STATUS_EVENT }) => void,
) => {
    return subscribe<WF_STATUS_EVENT>(clientId, groupId, CONFIG.topics.wfstatus, next)
}

export const subscribeUpdateHandler = async (
    clientId: string,
    groupId: string,
    next: (msg: { mesg: Message; data: UPDATE_EVENT_HANDLER }) => void,
) => {
    return subscribe<UPDATE_EVENT_HANDLER>(clientId, groupId, CONFIG.topics.update_event, next)
}

export const publishMutation = async (input: MUTATION_EVENT): Promise<any> => {
    const errors = schema.validate(input, schema.types.entity.entity_mutation)
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors)
        return errors
    }

    await publish(CONFIG.topics.mutation, input)
    return []
}

export const publishAudit = async (input: AUDIT_EVENT): Promise<any> => {
    const errors = schema.validate(input, schema.types.audit)
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors)
        return errors
    }

    await publish(CONFIG.topics.audit, input)
    return []
}

/*
  To publish the update event to handle the generic things over all services
  in input object has to send the event type, based on the event_type at each
  api end needs to be handled and in 'info' object we can send the required
  information.
*/

export const publishUpdateEvent = async (input: UPDATE_EVENT_HANDLER): Promise<any> => {
    const errors = schema.validate(input, schema.types.update_event)
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors)
        return errors
    }

    await publish(CONFIG.topics.update_event, input)
    return []
}

export const publishWfStatus = async (input: WF_STATUS_EVENT): Promise<any> => {
    const errors = schema.validate(input, schema.types.workflow.wf_status_event)
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors)
        return errors
    }

    await publish(CONFIG.topics.wfstatus, input)
    return []
}
