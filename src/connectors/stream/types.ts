/* eslint-disable camelcase */
/** *********************************************** */
// Mutation types
/** *********************************************** */
export type ENTITY = {
    id: string
    version_no: number
    version_user: string
    version_date: string
    [key: string]: any
}

export type MUTATION_EVENT = {
    type: string
    action: 'create' | 'update' | 'delete'
    record: ENTITY
    extra?: WF_DETAILS
}

/** *********************************************** */
// Audit types
/** *********************************************** */
export type AUDIT_EVENT = {
    source_id: string
    version_no: number
    prev_version_no?: number
}

/** *********************************************** */
// Workflow types
/** *********************************************** */
export type WF_DETAILS = {
    wf_conf_id: string
}

export type WF_STATUS_EVENT = {
    function: string
    sub_function: string
    source_id: string
    source_ref: string
    source_ref_updated: string
    status: string
    version_date: Date
}

/** *********************************************** */
// Update Event Handler Type
/** *********************************************** */

export type UPDATE_EVENT_HANDLER = {
    function: string
    source_id: string
    source_ref: string
    status: string
    event_type: string
    info: any
}
// To review if the below are required
export const MUTATION_ACTIONS = ['create', 'update', 'delete']
export type WF_PHASE = 'make' | 'review' | 'approve'
export type WF_APPROVE_STATUS = 'ACCEPTED' | 'REJECTED' | ''
