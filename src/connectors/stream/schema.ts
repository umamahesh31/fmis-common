import { Schema, Validator } from 'jsonschema'

// Utility packages - typescript-json-validator
/** *********************************************** */
// Entity schemas
/** *********************************************** */

const ENTITY_RECORD: Schema = {
    id: '/ENTITY_RECORD',
    type: 'object',
    properties: {
        id: { type: 'string' },
        version_no: { type: 'integer' },
        version_user: { type: 'string' },
        version_date: { type: 'date' },
    },
    required: ['id', 'version_no', 'version_user', 'version_date'],
    additionalProperties: true,
}

const ENTITY_MUTATION: Schema = {
    id: '/MUTATION_EVENT',
    type: 'object',
    properties: {
        type: { type: 'string' },
        action: { type: 'string', enum: ['create', 'delete', 'update'] },
        record: {
            $ref: '/ENTITY_RECORD',
        },
        extra: {
            $ref: '/WF_DETAILS',
        },
    },
    required: ['type', 'action', 'record'],
    additionalProperties: false,
}

/** *********************************************** */
// Workflow schemas
/** *********************************************** */

const WF_DETAILS: Schema = {
    id: '/WF_DETAILS',
    type: 'object',
    properties: {
        wf_conf_id: { type: 'string' },
    },
    required: ['wf_conf_id'],
    additionalProperties: false,
}

const WF_STATUS_EVENT: Schema = {
    id: '/WF_STATUS_EVENT',
    type: 'object',
    properties: {
        function: { type: 'string' },
        sub_function: { type: 'string' },
        source_id: { type: 'string' },
        source_ref: { type: 'string' },
        status: { type: 'string' },
        source_ref_updated: { type: 'string' },
        version_date: { type: 'date' },
    },
    required: ['function', 'source_id', 'source_ref', 'status'],
    additionalProperties: false,
}

const UPDATE_EVENT_HANDLER: Schema = {
    id: '/UPDATE_EVENT_HANDLER',
    type: 'object',
    properties: {
        function: { type: 'string' },
        source_id: { type: 'string' },
        source_ref: { type: 'string' },
        status: { type: 'string' },
        event_type: { type: 'string' },
        info: { type: 'any' },
    },
    required: ['event_type'],
    additionalProperties: false,
}

/** *********************************************** */
// Audit schemas
/** *********************************************** */

const AUDIT: Schema = {
    id: '/AUDIT',
    type: 'object',
    properties: {
        source_id: { type: 'string' },
        version_no: { type: 'integer' },
        prev_version_no: { type: 'integer' },
    },
    required: ['source_id', 'version_no'],
    additionalProperties: false,
}

/** *********************************************** */

const v = new Validator()
v.addSchema(ENTITY_RECORD)
v.addSchema(ENTITY_MUTATION)
v.addSchema(WF_DETAILS)
v.addSchema(WF_STATUS_EVENT)
v.addSchema(AUDIT)
v.addSchema(UPDATE_EVENT_HANDLER)

export const schema = {
    types: {
        entity: {
            entity_record: ENTITY_RECORD,
            entity_mutation: ENTITY_MUTATION,
        },
        workflow: {
            wf_details: WF_DETAILS,
            wf_status_event: WF_STATUS_EVENT,
        },
        audit: AUDIT,
        update_event: UPDATE_EVENT_HANDLER,
    },
    validate: (input: any, schema: Schema): any => {
        return v.validate(input, schema).errors
    },
}
