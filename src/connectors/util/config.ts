import { config, DotenvConfigOptions } from 'dotenv'
import * as fs from 'fs'
import * as path from 'path'
import { error } from '../../connectors'
let rawData: any

export const loadEnv = (options?: DotenvConfigOptions) => {
    config(options)
}

export function getValidationConfig(module: string) {
    const filePath = path.join('/app/config', 'validations.json')
    if (!rawData) {
        rawData = fs.readFileSync(filePath)
    }

    const obj = JSON.parse(rawData as any)
    if (!obj || !obj[module]) {
        console.log('no data found in validation found against: ', module)
        error.throwUserError('system.global.error')
    }
    return obj[module]
}
