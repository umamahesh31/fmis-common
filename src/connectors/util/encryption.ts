import { createCipher, createDecipher } from 'crypto'

export const CONFIG = {
    key: process.env.ENCRYPTION_KEY || 'd6F3Efeq',
    algorithm: 'aes-256-ctr',
}

export const encrypter = {
    encrypt: (input: string): any => {
        const cipher = createCipher(CONFIG.algorithm, CONFIG.key)
        let crypted = cipher.update(input, 'utf8', 'hex')
        crypted += cipher.final('hex')
        return crypted
    },
    decrypt: (encryptedText: string): any => {
        const decipher = createDecipher(CONFIG.algorithm, CONFIG.key)
        let dec = decipher.update(encryptedText, 'hex', 'utf8')
        dec += decipher.final('utf8')
        return dec
    },
}
