import { id } from './id'
import { CONFIG as ENC_CONFIG, encrypter } from './encryption'
import { loadEnv, getValidationConfig } from './config'
import { commons } from './commons'

const CONFIG = {
    ...ENC_CONFIG,
}

export const util = {
    CONFIG,
    encrypter,
    id,
    loadEnv,
    getValidationConfig,
    commons,
}
