import cuid from 'cuid'

export const id = {
    generate: (): string => {
        return cuid()
    },
}
