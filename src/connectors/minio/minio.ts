import { Client } from 'minio'
import { FILE_STATS } from './types'

let minioClient: Client

export function getClient() {
    if (minioClient != null) {
        return minioClient
    }
    minioClient = new Client({
        endPoint: process.env.MINIO_ENDPOINT || 'localhost',
        port: new Number(process.env.MINIO_PORT || '9000').valueOf(),
        useSSL: false,
        accessKey: process.env.MINIO_ACCESS_KEY || 'minio',
        secretKey: process.env.MINIO_SECRET_KEY || 'minio123',
    })
    return minioClient
}

export const upload = async (stream: any, uploadName: string): Promise<any> => {
    await getClient().putObject(getBucket(), uploadName, stream)
    const stats = await minioClient.statObject(getBucket(), uploadName)

    return stats as FILE_STATS
}

export const download = async (filename: string): Promise<any> => {
    return await getClient().getObject(getBucket(), filename)
}

export const remove = async (filename: string): Promise<any> => {
    return await getClient().removeObject(getBucket(), filename)
}

export function getBucket() {
    return process.env.MINIO_BUCKET || 'bucket'
}
