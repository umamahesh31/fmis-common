export interface FILE_METADATA {
    [key: string]: any
}

export type FILE_STATS = {
    size: number
    etag: string
    lastModified: Date
    metaData: FILE_METADATA
}
