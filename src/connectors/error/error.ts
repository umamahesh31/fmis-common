import { config } from 'dotenv'
import { AuthenticationError, ForbiddenError, UserInputError, ServiceError } from './types'

const props =
    config({
        path: 'messages.properties',
    }).parsed || {}

const format = function (template: string, ...params: any[]) {
    return template.replace(/{(\d+)}/g, function (match, number) {
        return typeof params[number] !== 'undefined' ? params[number] : match
    })
}

export const prepareMessage = (code: string, ...params: any[]): any => {
    const template = props[code]
    if (!template) {
        throw new ServiceError('Failed with unknown error code ' + code)
    }

    return format(template, ...params)
}

export const throwAuthError = (code: string, ...params: any[]): void => {
    const message = prepareMessage(code, ...params)
    throw new AuthenticationError(message)
}

export const throwForbiddenError = (code: string, ...params: any[]): void => {
    const message = prepareMessage(code, ...params)
    throw new ForbiddenError(message)
}

export const throwUserError = (code: string, ...params: any[]): void => {
    const message = prepareMessage(code, ...params)
    throw new UserInputError(message)
}

export const throwServiceError = (code: string, ...params: any[]): void => {
    const message = prepareMessage(code, ...params)
    throw new ServiceError(message)
}
