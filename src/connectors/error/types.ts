// Client Errors
export class UserInputError extends Error {
    status = 400
}
export class AuthenticationError extends Error {
    status = 401
}
export class ForbiddenError extends Error {
    status = 403
}

// Service Errors
export class ServiceError extends Error {
    status = 500
}
