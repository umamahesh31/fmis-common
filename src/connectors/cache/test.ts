import { cache } from '.'
import { deepStrictEqual, strictEqual, equal } from 'assert'

async function test() {
  const input1 = 'hello'
  await cache.set('strkey', input1)
  const output1 = await cache.get('strkey')
  strictEqual(input1, output1)

  const input2 = 10
  await cache.set('numkey', 10)
  const output2 = await cache.get('numkey')
  equal(input2, output2)

  const input3 = { a: 10 }
  await cache.set('objkey', input3)
  const output3 = await cache.get('objkey')
  strictEqual(JSON.stringify(input3), output3)

  const output4 = await cache.get_as_obj('objkey')
  deepStrictEqual(output4, input3)

  const output5 = await cache.exists('objkey')
  strictEqual(output5, true)

  const output6 = await cache.exists('nonexistentkey')
  strictEqual(output6, false)

  const input7 = 'user1 value'
  const input8 = 'user2 value'
  await cache.set('user.1', input7)
  await cache.set('user.2', input8)
  await cache.clear_all('user*')
  const output7 = await cache.exists('user.1')
  const output8 = await cache.exists('user.2')
  strictEqual(output7, false)
  strictEqual(output8, false)

  const input9 = 'user1 value'
  await cache.set('userkey', input9)
  await cache.clear('userkey')
  const output9 = await cache.exists('userkey')
  strictEqual(output9, false)

  console.log('all test cases passed')
}

test()
