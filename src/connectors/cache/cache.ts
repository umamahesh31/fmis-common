import { Tedis, TedisPool } from 'tedis'
import { getVersion } from '../stream/natsstream'

let tedis1: Tedis

// TODO: check on how to auto reconnect
const connect = async () => {
    if (tedis1) {
        return tedis1
    } else {
        // const tedispool = new TedisPool({
        //     host: process.env.REDIS_HOST || 'localhost',
        //     port: parseInt(process.env.REDIS_PORT || '') || 6379,
        // })

        // const tedis = await tedispool.getTedis()
        // tedispool.putTedis(tedis)
        // tedis1 = tedis

        tedis1 = new Tedis({
            host: process.env.REDIS_HOST || 'localhost',
            port: parseInt(process.env.REDIS_PORT || '') || 6379,
        })
        let tds: Tedis

        tedis1.on('connect', () => {
            console.log('connect')
        })
        tedis1.on('timeout', () => {
            console.log('timeout')
        })
        tedis1.on('error', (err) => {
            console.log(err)
            tedis1 = tds
        })
        tedis1.on('close', (had_error) => {
            console.log('close with err: ', had_error)
            tedis1 = tds
        })
        return tedis1
    }
}

export const cacheConnect = async () => {
    return await connect()
}

export const get = async (key: string): Promise<any> => {
    return await (await connect()).get(includeVersionKey(key))
}

export const get_as_obj = async (key: string): Promise<any> => {
    const value = await (await connect()).get(includeVersionKey(key))
    return value && typeof value === 'string' ? JSON.parse(value) : value
}

export const set = async (key: string, value: any, expiryMins = 0): Promise<any> => {
    const tds = await connect()
    key = includeVersionKey(key)
    await tds.set(key, typeof value === 'object' ? JSON.stringify(value) : value)

    if (expiryMins > 0) {
        await tds.expire(key, expiryMins * 60)
    }
}

export const mset = async (info: any, versionKeyIncluded: boolean): Promise<any> => {
    const tds = await connect()
    const newMapObj: any = {}
    if (versionKeyIncluded) {
        for (const property in info) {
            newMapObj[includeVersionKey(property)] = info[property]
        }
        await tds.mset(newMapObj)
    } else {
        await tds.mset(info)
    }
}

export const exists = async (key: string): Promise<boolean> => {
    const tds = await connect()
    return (await tds.exists(includeVersionKey(key))) === 1
}

export const clear = async (key: string): Promise<void> => {
    const tds = await connect()
    await tds.del(includeVersionKey(key))
}

export const clear_all = async (keyPattern: string): Promise<void> => {
    const tds = await connect()
    const keys = await tds.keys(keyPattern)
    let keysToDelete = []
    const batchSize = process.env.REDIS_BATCH_SIZE || 10000
    for (const key of keys) {
        keysToDelete.push(key)
        if (keysToDelete.length === batchSize) {
            await tds.del('', ...keysToDelete)
            keysToDelete = []
        }
    }
    if (keysToDelete.length > 0) {
        await tds.del('', ...keysToDelete)
    }
}

export const flush_all = async (): Promise<void> => {
    const tds = await connect()
    await tds.command('FLUSHALL')
}

export const get_keys = async (keyPattern: string): Promise<any> => {
    const tds = await connect()
    return await tds.keys(keyPattern)
}

export const includeVersionKey = (key: string): string => {
    const versionKey = getVersion().toUpperCase()
    key = versionKey + key
    return key
}
export const set_ttl = async (key: string, expireTime: string) => {
    const tds = await connect()
    await tds.set(includeVersionKey(key), expireTime)
}
export const get_ttl = async (key: string): Promise<number> => {
    const tds = await connect()
    return await tds.ttl(includeVersionKey(key))
}

export const hdel = async (key: string, field: string, ...fields: string[]): Promise<number> => {
    return await (await connect()).hdel(includeVersionKey(key), field, ...fields)
}
export const hexists = async (key: string, field: string): Promise<number> => {
    return await (await connect()).hexists(includeVersionKey(key), field)
}
export const hget = async (key: string, value: string): Promise<string | null> => {
    return await (await connect()).hget(includeVersionKey(key), value)
}
export const hgetall = async (key: string): Promise<{ [propName: string]: string }> => {
    return await (await connect()).hgetall(includeVersionKey(key))
}
export const hincrby = async (key: string, field: string, increment: number): Promise<number> => {
    return await (await connect()).hincrby(includeVersionKey(key), field, increment)
}

export const hincrbyfloat = async (key: string, field: string, increment: number): Promise<string> => {
    return await (await connect()).hincrbyfloat(includeVersionKey(key), field, increment)
}

export const hkeys = async (key: string): Promise<string[]> => {
    return await (await connect()).hkeys(includeVersionKey(key))
}
export const hlen = async (key: string): Promise<number> => {
    return await (await connect()).hlen(includeVersionKey(key))
}
export const hmget = async (key: string, field: string, ...fields: string[]): Promise<Array<string | null>> => {
    return await (await connect()).hmget(includeVersionKey(key), field, ...fields)
}

export const hmset = async (key: string, hash: { [propName: string]: string | number }): Promise<any> => {
    return await (await connect()).hmset(includeVersionKey(key), hash)
}
export const hset = async (key: string, field: string, value: string): Promise<0 | 1> => {
    return await (await connect()).hset(includeVersionKey(key), field, value)
}

export const hsetnx = async (key: string, field: string, value: string): Promise<0 | 1> => {
    return await (await connect()).hsetnx(includeVersionKey(key), field, value)
}

export const hstrlen = async (key: string, field: string): Promise<number> => {
    return await (await connect()).hstrlen(includeVersionKey(key), field)
}
export const hvals = async (key: string): Promise<string[]> => {
    return await (await connect()).hvals(includeVersionKey(key))
}
export const ttl = async (key: string): Promise<number> => {
    return await (await connect()).ttl(includeVersionKey(key))
}

export const expire = async (key: string, seconds: number): Promise<number> => {
    return await (await connect()).expire(includeVersionKey(key), seconds)
}
export const del = async (key: string, ...keys: string[]): Promise<number> => {
    return await (await connect()).del(includeVersionKey(key), ...keys)
}
export const expireat = async (key: string, timestamp: number): Promise<number> => {
    return await (await connect()).expireat(includeVersionKey(key), timestamp)
}
export const keys = async (pattern: string): Promise<string[]> => {
    return await (await connect()).keys(pattern)
}
export const move = async (key: string, db: number): Promise<number> => {
    return await (await connect()).move(includeVersionKey(key), db)
}
export const persist = async (key: string): Promise<number> => {
    return await (await connect()).persist(includeVersionKey(key))
}
export const pexpire = async (key: string, milliseconds: number): Promise<number> => {
    return await (await connect()).pexpire(includeVersionKey(key), milliseconds)
}
export const pexpireat = async (key: string, millisecondsTimestamp: number): Promise<number> => {
    return await (await connect()).pexpireat(includeVersionKey(key), millisecondsTimestamp)
}
export const pttl = async (key: string): Promise<number> => {
    return await (await connect()).pttl(includeVersionKey(key))
}
export const randomkey = async (): Promise<null | string> => {
    return await (await connect()).randomkey()
}
export const renamenx = async (key: string, newKey: string): Promise<0 | 1> => {
    return await (await connect()).renamenx(includeVersionKey(key), newKey)
}
export const type = async (key: string): Promise<string> => {
    return await (await connect()).type(key)
}
