const Email = require('email-templates')
const nodemailer = require('nodemailer')
const path = require('path')

let mailTemplate: any

const getMailTemplate = () => {
    const transport = nodemailer.createTransport({
        host: process.env.SMTP_HOST || '',
        port: new Number(process.env.SMTP_PORT || '465').valueOf(),
        secure: new Boolean(process.env.SMTP_SECURE || false).valueOf(),
        auth: {
            user: process.env.SMTP_USER || '',
            pass: process.env.SMTP_PASSWORD || '',
        },
        debug: true,
    })

    return new Email({
        message: {
            from: process.env.SMTP_FROM || '',
        },
        send: true,
        transport: transport,
        preview: false,
    })
}

export const send = (template: string, data: any, to: string): void => {
    if (!mailTemplate) mailTemplate = getMailTemplate()
    mailTemplate
        .send({
            template: path.join('/app/config', 'emails', template),
            message: {
                to: to,
                attachments: data.attachments,
            },
            locals: data,
        })
        .then((res: any) => {
            // console.log('Response for sending the email', res.originalMessage)
        })
        .catch((err: any) => console.log(err))
}
