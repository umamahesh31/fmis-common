import { sign, verify, decode } from 'jsonwebtoken'
import * as bcrypt from 'bcryptjs'
import { AuthToken } from './types'

export const encrypt = (userId: string, username: string, email: string, tenantId: string): any => {
    const expires: any = { expiresIn: 60 * 60 }
    return sign(
        {
            userId,
            username,
            email,
            tenantId,
        } as AuthToken,
        process.env.JWT_SECRET || '',
        expires,
    )
}

export const deencrypt = (token: string): any => {
    return verify(token, process.env.JWT_SECRET || '') as AuthToken
}

export const refresh = (token: string): any => {
    const data = decode(token) as AuthToken
    return encrypt(data.userId, data.username, data.email, data.tenantId)
}

export const hash = (password: string): any => {
    return bcrypt.hashSync(password, 10)
}

export const hashCompare = (password1: string, password2: string): any => {
    return bcrypt.compare(password1, password2)
}
