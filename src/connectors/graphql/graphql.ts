import { GraphQLClient } from 'graphql-request'
import { AuthToken } from '../auth'
import { Headers } from 'cross-fetch'

export const request = (apiURL: string, query: string, variables: any, rawToken: string, token: AuthToken): any => {
    const client = new GraphQLClient(apiURL)

    global.Headers = global.Headers || Headers

    if (token) {
        client.setHeader('x-email', token.email)
        client.setHeader('x-tenantid', token.tenantId)
        client.setHeader('x-userid', token.userId)
        client.setHeader('x-username', token.username)
    }
    client.setHeader('x-internal', 'y')
    client.setHeader('authorization', rawToken)

    return client.request(query, variables)
}
