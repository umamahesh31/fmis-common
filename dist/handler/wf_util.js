"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.canSaveAllowed = exports.validateNGetWfParams = exports.wfConfId = void 0;
const connectors_1 = require("../connectors");
const wfConfId = async (confId, officeId, ctx) => {
    const query = `
    query wfConfId($wf_conf_id: String!, $office_code: String!) {
        wfConfId(wf_conf_id: $wf_conf_id, office_code: $office_code)
      }
    `;
    const result = await connectors_1.api.request(process.env.EVENT_API_URL || '', query, { wf_conf_id: confId, office_code: officeId }, ctx.rawToken, ctx.token);
    return result.wfConfId;
};
exports.wfConfId = wfConfId;
const validateNGetWfParams = (args) => {
    var _a, _b, _c;
    // if (!args.data?.wf_params && !(args.data?.is_latest?.set || args.data?.status?.set === 'Approved')) { //TOFO NEED ADD BY IMPROVE
    //     error.throwUserError('Workflow Params are required to take action')
    // }
    if (!((_a = args.data) === null || _a === void 0 ? void 0 : _a.wf_params)) {
        return undefined;
    }
    const wfParams = (_b = args.data) === null || _b === void 0 ? void 0 : _b.wf_params;
    (_c = args.data) === null || _c === void 0 ? true : delete _c.wf_params;
    if (wfParams && !wfParams.fmis_function) {
        connectors_1.error.throwUserError('FMIS Function is required to take action');
    }
    if (wfParams && !wfParams.wf_conf_id) {
        connectors_1.error.throwUserError('Workflow ID is required to take action');
    }
    if (!wfParams.assign_to) {
        connectors_1.error.throwUserError('AssignTo is required to take action');
    }
    if (!wfParams.office_code) {
        connectors_1.error.throwUserError('Office Code is required to take action');
    }
    return wfParams;
};
exports.validateNGetWfParams = validateNGetWfParams;
const canSaveAllowed = async (ctx, functionCode, officeCode, wfParams) => {
    if (!(await ctx.isAuthorized(ctx.token.username, functionCode + '_U', officeCode))) {
        return false;
    }
    return true;
};
exports.canSaveAllowed = canSaveAllowed;
//# sourceMappingURL=wf_util.js.map