export declare function publishAudit(wfParams: any | undefined, type: string, record: any, action: 'create' | 'update' | 'delete', rawToken: string, token: any, fmisSubFunction?: string, isBulkCreate?: boolean): Promise<void>;
export declare function createWf(variables: {
    [P in string]: any;
}, rawToken: string, token: any): Promise<any>;
export declare function submitWf(variables: {
    [P in string]: any;
}, rawToken: string, token: any): Promise<any>;
export declare function getFunctionTableMap(): Map<string, string>;
