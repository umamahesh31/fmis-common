export declare const wfConfId: (confId: string, officeId: string, ctx: any) => Promise<any>;
export declare const validateNGetWfParams: (args: any) => any;
export declare const canSaveAllowed: (ctx: any, functionCode: string, officeCode: string, wfParams: any) => Promise<boolean>;
