"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFunctionTableMap = exports.submitWf = exports.createWf = exports.publishAudit = void 0;
const connectors_1 = require("../connectors");
async function publishAudit(
// eslint-disable-next-line camelcase
wfParams, type, record, action, rawToken, token, fmisSubFunction, isBulkCreate) {
    const mutationObject = {
        type: type,
        action: action,
        record: record,
    };
    console.log('Audit Event Request Received:', mutationObject);
    if (wfParams && typeof wfParams === 'object' && wfParams !== null) {
        switch (action) {
            case 'create':
                {
                    let wfVariables = wfParams;
                    if (isBulkCreate) {
                        const list = [];
                        for (const data of record) {
                            list.push(handleBulkCreate(wfParams, data, fmisSubFunction));
                        }
                        wfVariables['list'] = list;
                        wfVariables['bulk_create'] = true;
                    }
                    else {
                        wfVariables = handleBulkCreate(wfParams, record, fmisSubFunction);
                        wfVariables['seq_key'] = wfParams.seq_key;
                        wfVariables['seq_conf'] = wfParams.seq_conf;
                    }
                    await createWf(wfVariables, rawToken, token);
                }
                break;
            case 'update':
                {
                    const wfUpdateVariables = wfParams;
                    wfUpdateVariables['source_id'] = record.id;
                    wfUpdateVariables['source_version'] = record.version_no;
                    wfUpdateVariables['status'] = wfParams.action || 'ACCEPTED';
                    wfUpdateVariables['comments'] = wfParams.comments || '';
                    wfUpdateVariables['assignedTo'] = wfParams.assign_to;
                    wfUpdateVariables['office_code'] = wfParams.office_code || '';
                    await submitWf(wfUpdateVariables, rawToken, token);
                }
                break;
            case 'delete':
                {
                    const deleteVariables = wfParams;
                    deleteVariables['source_id'] = record.id;
                    deleteVariables['source_version'] = record.version_no;
                    deleteVariables['status'] = 'CANCEL';
                    deleteVariables['comments'] = 'AUTO CANCELLED FROM SOURCE DELETION:' + record.id;
                    deleteVariables['assignedTo'] = record.version_user || 'SYSTEM';
                    deleteVariables['office_code'] = wfParams.office_code || '';
                    await submitWf(deleteVariables, rawToken, token);
                }
                break;
        }
    }
    if (wfParams && !wfParams.byPassAudit) {
        if (isBulkCreate) {
            for (const data of record) {
                mutationObject.record = data;
                await connectors_1.stream.publishMutation(mutationObject);
            }
        }
        else {
            await connectors_1.stream.publishMutation(mutationObject);
        }
    }
}
exports.publishAudit = publishAudit;
function handleBulkCreate(wfParams, record, fmisSubFunction) {
    const wfVariables = {};
    wfVariables['wf_conf_id'] = wfParams.wf_conf_id;
    wfVariables['source_id'] = record.id;
    wfVariables['source_ref'] = record.ref;
    wfVariables['source_version'] = record.version_no;
    wfVariables['office_code'] = wfParams.office_code || '';
    wfVariables['assign_to'] = wfParams.assign_to;
    wfVariables['fmis_function'] = wfParams.fmis_function;
    wfVariables['fmis_sub_function'] = fmisSubFunction || 'NA';
    wfVariables['comments'] = wfParams.comments || '';
    wfVariables['is_submit'] = true;
    return wfVariables;
}
async function createWf(variables, rawToken, token) {
    const mutation = `
    mutation wfcreate($wf_params:Json!) {
        wfcreate(wf_params:$wf_params)
      }
    `;
    const result = await connectors_1.api.request(process.env.EVENT_API_URL || '', mutation, { wf_params: variables }, rawToken, token);
    console.log('Workflow instance creation response:', result.wfcreate);
    return result.wfcreate;
}
exports.createWf = createWf;
async function submitWf(variables, rawToken, token) {
    const mutation = `
    mutation wfsubmit($wf_params:Json!){
        wfsubmit(wf_params:$wf_params)
  }`;
    let result;
    try {
        result = await connectors_1.api.request(process.env.EVENT_API_URL || '', mutation, { wf_params: variables }, rawToken, token);
    }
    catch (err) {
        console.log('error occurred while submit workflow ', err);
        throw Error(err.response.errors[0].message || '');
    }
    console.log('Workflow instance submit response:', result.wfsubmit);
    return result.wfsubmit;
}
exports.submitWf = submitWf;
function getFunctionTableMap() {
    const map = new Map();
    // CONTRACT MANAGEMENT
    map.set('SYS_USER_MASTER', 'public."User"');
    map.set('SYS_ROLE_MASTER', 'public."Role"');
    map.set('COA_ECONOMIC_CODES_MASTER', 'public."EconomicClassifCode"');
    // CONTRACT MANAGEMENT
    map.set('CNTR_SUPPLIER_CTGRY_MASTER', 'public."CntrSupplierCategory"');
    map.set('CNTR_SUPPLIER_REGISTRATION', 'public."CntrSupplier"');
    map.set('CNTR_CONTRACT_CREATION', 'public."CntrContract"');
    // EXPENDITURE MANAGEMENT
    map.set('EXP_CONTRACT_RUNNING_BILL', 'public."ContractBill"');
    map.set('EXP_CONTRACT_ADVANCE_BILL', 'public."ContractBill"');
    map.set('EXP_CONTRACT_FINAL_BILL', 'public."ContractBill"');
    map.set('EXP_CONTRACT_SEC_DEPOSIT_REFUND', 'public."ContractBill"');
    map.set('EXP_CONTRACT_BILL_CREATION', 'public."ContractBill"');
    map.set('EXP_OTHER_BILL_CREATION', 'public."OtherBill"');
    map.set('EXP_IMPREST_RELEASE_CREATION', 'public."ImprestRelease"');
    map.set('EXP_IMPREST_ADJ_CREATION', 'public."ImprestAdjustment"');
    // PAYMENT MANAGEMENT
    map.set('PAYMENT_INSTRUCTION', 'public."PaymentInstruction"');
    // GENERAL LEDGER
    map.set('JOURNAL_VOUCHER', 'public."PaymentInstruction"');
    map.set('COA_SEGMENT_CONF_MASTER', 'public."PaymentInstruction"');
    return map;
}
exports.getFunctionTableMap = getFunctionTableMap;
//# sourceMappingURL=audit_handler.js.map