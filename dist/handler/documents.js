"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateMultipleDocuments = exports.generateDocuments = void 0;
const connectors_1 = require("../connectors");
const generateDocuments = async (sourceId, documents, rawToken, token) => {
    if (!documents || !documents.data || documents.data.length === 0) {
        return;
    }
    for (const doc of documents.data) {
        doc['source_id'] = sourceId;
    }
    return await addDocuments(documents, rawToken, token);
};
exports.generateDocuments = generateDocuments;
async function addDocuments(documents, rawToken, token) {
    const mutation = `
    mutation createManyDocument($data: DocumentBulkCreateInput!) {
        createManyDocument(data: $data)
      }
    `;
    const result = await connectors_1.api.request(process.env.SYS_ADM_API_URL || '', mutation, { data: { documents: documents.data } }, rawToken, token);
    console.log('Documents creation response:', result);
    return result;
}
const generateMultipleDocuments = async (sourceIdVsDocumentsMap, rawToken, token) => {
    const documentsData = [];
    for (const [sourceId, documents] of sourceIdVsDocumentsMap) {
        if (!documents || !documents.data || documents.data.length === 0) {
            return;
        }
        for (const doc of documents.data) {
            doc['source_id'] = sourceId;
        }
        documentsData.push(documents.data);
    }
    return await addMultipleDocuments(documentsData, rawToken, token);
};
exports.generateMultipleDocuments = generateMultipleDocuments;
async function addMultipleDocuments(documents, rawToken, token) {
    const mutation = `
    mutation createManyDocument($data: DocumentBulkCreateInput!) {
        createManyDocument(data: $data)
      }
    `;
    const result = await connectors_1.api.request(process.env.SYS_ADM_API_URL || '', mutation, { data: { documents: documents } }, rawToken, token);
    console.log('Documents creation response:', result);
    return result;
}
//# sourceMappingURL=documents.js.map