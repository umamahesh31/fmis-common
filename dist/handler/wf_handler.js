"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initHandler = void 0;
const connectors_1 = require("../connectors");
const client_1 = require("@prisma/client");
const initHandler = async (clientId, groupId) => {
    console.log('server:', process.env.NATS_STREAM_URL);
    const db = new client_1.PrismaClient();
    await connectors_1.stream.subscribeWfUpdate(clientId, groupId, async (msg) => {
        console.log('----------------------------------------');
        const seq = msg.mesg.getSequence().toString();
        console.log(`${seq}.${clientId}. received request: ${msg.data}`);
        try {
            await handle(seq, msg.data);
        }
        catch (e) {
            return;
        }
        msg.mesg.ack();
        console.log('----------------------------------------');
    });
    const handle = async (seq, input) => {
        try {
            await process1(input);
            console.log(`${seq}.${clientId}. finished`);
        }
        catch (err) {
            console.error(`${seq}.${clientId}. Error: ${err.message}`);
            console.log(`${seq}.${clientId}. failed`);
            throw err;
        }
    };
    const process1 = async (input) => {
        const functionTableMap = getFunctionTableMap();
        let tableName = '';
        tableName = functionTableMap.get(input.function) || '';
        if (tableName !== '') {
            let updateQry1 = `UPDATE ${tableName}  SET is_latest=FALSE, is_effective=FALSE WHERE ref='${input.source_ref}'`;
            if (input.function === 'SUPPLIER_CATEGORY') {
                updateQry1 = `UPDATE ${tableName}  SET is_latest=FALSE, is_effective=FALSE WHERE name='${input.source_ref}'`;
            }
            const updateQry2 = `UPDATE ${tableName}  SET status='${input.status}', is_latest=TRUE, is_effective=is_active WHERE id='${input.source_id}'`;
            try {
                await db.$executeRaw(updateQry1);
                await db.$executeRaw(updateQry2);
                // await db.$transaction([qry1, qry2])
            }
            catch (error) {
                console.log('Error occurred while updating WF status', error);
                console.log('Updates:', updateQry1, updateQry2);
                throw error;
            }
        }
    };
    function getFunctionTableMap() {
        const map = new Map();
        map.set('SUPPLIER_CATEGORY', 'public."CntrSupplierCategory"');
        map.set('SUPPLIER_REGISTRATION', 'public."CntrSupplier"');
        map.set('CONTRACT_CREATION', 'public.CntrContract"');
        map.set('CONTRACT_BILL', 'public.ContractBill"');
        map.set('OTHER_BILL', 'public.OtherBill"');
        map.set('IMPREST_RELEASE', 'public.ImprestRelease"');
        map.set('IMPREST_ADJUSTMENT', 'public.ImprestAdjustment"');
        map.set('PAYMENT_INSTRUCTION', 'public.PaymentInstruction"');
        return map;
    }
};
exports.initHandler = initHandler;
//# sourceMappingURL=wf_handler.js.map