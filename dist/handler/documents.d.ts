export declare const generateDocuments: (sourceId: string, documents: any, rawToken: string, token: any) => Promise<void>;
export declare const generateMultipleDocuments: (sourceIdVsDocumentsMap: Map<string, any>, rawToken: string, token: any) => Promise<void>;
