import { Message } from 'node-nats-streaming';
import { BILL_EVENT } from './types';
export declare const CONFIG: {
    topics: {
        mutation: string;
        wfstatus: string;
        audit: string;
        bill: string;
        update_event: string;
    };
};
export declare const subscribeBill: (clientId: string, groupId: string, next: (msg: {
    mesg: Message;
    data: BILL_EVENT;
}) => void) => Promise<void>;
export declare const publishBill: (input: BILL_EVENT) => Promise<import("jsonschema").ValidationError[]>;
