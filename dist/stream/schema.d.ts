import { Schema } from 'jsonschema';
export declare const schema: {
    types: {
        payment: {
            bill: Schema;
        };
    };
    validate: (input: any, schema: Schema) => import("jsonschema").ValidationError[];
};
