"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.schema = void 0;
const jsonschema_1 = require("jsonschema");
/** *********************************************** */
// Bill Payment schemas
/** *********************************************** */
const bill = {
    id: '/BILL',
    type: 'object',
    properties: {
        tenant_id: { type: 'string' },
        dept_id: { type: 'string' },
        source_id: { type: 'string' },
        type: { type: 'string' },
        amount: { type: 'string' },
        mode: { type: 'string' },
        version_user: { type: 'string' },
    },
    required: ['tenant_id', 'dept_id', 'source_id', 'type', 'amount', 'mode', 'version_user'],
    additionalProperties: false,
};
// TO-DO need to add payee_details to bill schema
// payee_details:[{
//   payee_name     String
//   acc_num        String
//   bank_id        String
//   amount         Float
// }]
/** *********************************************** */
const v = new jsonschema_1.Validator();
v.addSchema(bill);
exports.schema = {
    types: {
        payment: {
            bill,
        },
    },
    validate: (input, schema) => {
        return v.validate(input, schema).errors;
    },
};
//# sourceMappingURL=schema.js.map