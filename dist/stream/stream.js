"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publishBill = exports.subscribeBill = exports.CONFIG = void 0;
const connectors_1 = require("../connectors");
const schema_1 = require("./schema");
exports.CONFIG = {
    topics: {
        ...connectors_1.stream.CONFIG.topics,
    },
};
/* *************************************************
 Bill Payment streams
****************************************************/
const subscribeBill = async (clientId, groupId, next) => {
    return await connectors_1.stream.subscribe(clientId, groupId, exports.CONFIG.topics.bill, next);
};
exports.subscribeBill = subscribeBill;
const publishBill = async (input) => {
    const errors = await schema_1.schema.validate(input, schema_1.schema.types.payment.bill);
    if (errors.length > 0) {
        return errors;
    }
    await connectors_1.stream.publish(exports.CONFIG.topics.bill, input);
    return [];
};
exports.publishBill = publishBill;
//# sourceMappingURL=stream.js.map