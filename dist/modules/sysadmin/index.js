"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sysadmin = void 0;
const fin_year_1 = require("./fin_year");
const currency_1 = require("./currency");
exports.sysadmin = {
    GetFinYearForSeqGeneration: fin_year_1.GetFinYearForSeqGeneration,
    GetMeTheCurrentFinYearID: fin_year_1.GetMeTheCurrentFinYearID,
    GetCurrency: currency_1.GetCurrency,
};
//# sourceMappingURL=index.js.map