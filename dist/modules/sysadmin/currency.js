"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSetting = exports.GetCurrency = void 0;
const connectors_1 = require("../../connectors");
async function GetCurrency(ctx, currency) {
    await getSetting(ctx, currency);
    return connectors_1.cache.get(currency);
}
exports.GetCurrency = GetCurrency;
async function getSetting(ctx, currency) {
    var _a, _b;
    const query = `query appSetting($where:AppSettingWhereUniqueInput!){
  appSetting(where:$where){
  value1
  }
}`;
    const result = await connectors_1.api.request(process.env.SYS_ADM_API_URL || '', query, { where: { id: currency } }, ctx.rawToken || '', ctx.token);
    const countryQuery = `query Countries($where:CountryWhereInput){
      countries(where:$where){
        ccy
      }
      }`;
    const countryResult = await connectors_1.api.request(process.env.SYS_ADM_API_URL || '', countryQuery, { where: { ccy_code: { equals: (_a = result === null || result === void 0 ? void 0 : result.appSetting) === null || _a === void 0 ? void 0 : _a.value1 } } }, ctx.rawToken || '', ctx.token);
    await connectors_1.cache.set(currency, (_b = countryResult === null || countryResult === void 0 ? void 0 : countryResult.countries[0]) === null || _b === void 0 ? void 0 : _b.ccy);
}
exports.getSetting = getSetting;
//# sourceMappingURL=currency.js.map