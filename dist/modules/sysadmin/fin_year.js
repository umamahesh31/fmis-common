"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetFinYearForSeqGeneration = exports.GetMeTheCurrentFinYearID = void 0;
const connectors_1 = require("../../connectors");
async function GetMeTheCurrentFinYearID(ctx) {
    const yearId = await connectors_1.cache.get('GetMeTheCurrentFinYearID');
    if (yearId) {
        return yearId;
    }
    const currentDate = new Date();
    const variables = {
        currentDate: currentDate,
    };
    const query = `query admFinYears($currentDate:DateTime!){
      admFinYears(where:{
        start_date: { lte: $currentDate },
        end_date: { gte: $currentDate }
      }
      ){
        id
      }
    }`;
    const result = await connectors_1.api.request(process.env.SYS_ADM_API_URL || '', query, variables, ctx.rawToken, ctx.token);
    if (result.admFinYears && result.admFinYears.length > 0) {
        connectors_1.cache.set('GetMeTheCurrentFinYearID', result.admFinYears[0].id);
        return result.admFinYears[0].id;
    }
    return '';
}
exports.GetMeTheCurrentFinYearID = GetMeTheCurrentFinYearID;
// GetFinYearForSeqGeneration -To get the fin year for the reference sequence generation
async function GetFinYearForSeqGeneration(ctx) {
    let yearID = '';
    yearID = await GetMeTheCurrentFinYearID(ctx);
    if (!yearID || yearID === '') {
        return yearID;
    }
    const years = yearID.split('-');
    return years[0].substr(2, 2) + '-' + years[1].substr(2, 2);
}
exports.GetFinYearForSeqGeneration = GetFinYearForSeqGeneration;
//# sourceMappingURL=fin_year.js.map