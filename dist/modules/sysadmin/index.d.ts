import { GetFinYearForSeqGeneration, GetMeTheCurrentFinYearID } from './fin_year';
import { GetCurrency } from './currency';
export declare const sysadmin: {
    GetFinYearForSeqGeneration: typeof GetFinYearForSeqGeneration;
    GetMeTheCurrentFinYearID: typeof GetMeTheCurrentFinYearID;
    GetCurrency: typeof GetCurrency;
};
