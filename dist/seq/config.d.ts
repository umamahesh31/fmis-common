import { SEQ_CONFIG, WF_DETAIL } from './types';
export declare const seqConfig: Record<string, SEQ_CONFIG>;
export declare const workflowConfig: Record<string, WF_DETAIL>;
