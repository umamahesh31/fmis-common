export declare const CONFIG: {
    get: (type: string) => any;
};
export declare const generate: (type: string, record: Record<string, any>, ctx: any) => Promise<any>;
export declare const wfSeq: (seqId: string) => any;
export declare const generateAlphaSeq: (seq: any, config: any, key: string, value: string, ctx: any) => Promise<any>;
