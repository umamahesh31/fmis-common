export declare type SEQ_CONFIG = {
    type: string;
    key_template: string;
    value_template: string;
    value_template_details?: any;
    input_fields: string;
    dynamic_fields: string;
    start_value: number;
    seq_size: number;
};
export declare type WF_DETAIL = {
    seqType: string;
    function_name: string;
    module_name: string;
};
