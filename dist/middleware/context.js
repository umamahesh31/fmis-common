"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isCiauUser = exports.isConcurrentApprover = exports.isSuperAdmin = exports.makeContext = void 0;
const connectors_1 = require("../connectors");
const SUPER_ADMIN = 'SUPER_ADMIN';
const CONCURRENT_APPROVER = 'CONCURRENT_APPROVER';
const GLOBAL_CIAU = 'GLOBAL_CIAU';
const getTenant = (token) => {
    if (token && token.tenantId) {
        return token.tenantId;
    }
    return process.env.DEFAULT_TENANT || 'TEN1';
};
const readRawToken = (params) => {
    params.res.set({ 'Access-Control-Expose-Headers': 'x-refresh-token,x-server-date' });
    params.res.setHeader('x-server-date', new Date().toISOString());
    if (params.req.headers.authorization && params.req.headers.authorization.split(' ')[0] === 'Bearer') {
        try {
            return connectors_1.auth.deencrypt(params.req.headers.authorization.split(' ')[1]);
        }
        catch (err) {
            if (err.message === 'jwt expired') {
                const token = connectors_1.auth.refresh(params.req.headers.authorization.split(' ')[1]);
                params.req.headers.authorization = 'Bearer ' + token;
                params.res.setHeader('x-refresh-token', token);
                return readRawToken(params);
            }
            console.log('Failed to decrypt auth token', err);
        }
    }
    return undefined;
};
const readFromHeaders = (req) => {
    return {
        email: req.headers['x-email'],
        tenantId: req.headers['x-tenantid'],
        userId: req.headers['x-userid'],
        username: req.headers['x-username'],
    };
};
const getToken = (params) => {
    if (params.req.headers['x-external'] === 'Y') {
        return readRawToken(params);
    }
    else {
        return readFromHeaders(params.req);
    }
};
const isAuthorized = async (username, functionCode, ...dataCodes) => {
    if (functionCode.endsWith('_C')) {
        if (await canHaveAccess(username, functionCode, ...dataCodes)) {
            return true;
        }
        else {
            return await canHaveAccess(username, functionCode.slice(0, -1) + 'U', ...dataCodes);
        }
    }
    else {
        return await canHaveAccess(username, functionCode, ...dataCodes);
    }
};
const canHaveAccess = async (username, functionCode, ...dataCodes) => {
    const canAccessAll = await connectors_1.cache.exists(username + '#' + functionCode + '#' + 'ALL'); // If the user has access for ALL, Only one entry will be there
    if (canAccessAll) {
        console.log(`${username + '#' + functionCode + '#' + 'ALL'} Can access all the keys.`);
        return true;
    }
    if (dataCodes[0] === 'ALL') {
        // If the privilege is of GLOBAL and the role is of NON-GLOBAL, We should allow when the user has access for any
        const offices = await connectors_1.cache.get(username + '#' + functionCode + '#');
        if (offices) {
            console.log(`${username + '#' + functionCode + '#'} codes have the office code ALL`);
            return true;
        }
    }
    for (const dataCode of dataCodes) {
        // Checking All office codes Access -If the list is multiple
        console.log('data code key:: ', dataCode);
        const dataCodeAccess = await connectors_1.cache.exists(username + '#' + functionCode + '#' + dataCode);
        console.log(`${username + '#' + functionCode + '#'} has the access on office code ${dataCode}`, dataCodeAccess);
        if (!dataCodeAccess) {
            return false;
        }
    }
    return true;
};
const dataCodes = async (username, functionCode) => {
    return await connectors_1.cache.get(username + '#' + functionCode + '#');
};
const applyDataCodes = async (username, functionCode, isInternal, args, ...dataFields) => {
    var _a;
    if (isInternal || (dataFields && dataFields[0] === 'ALL')) {
        return;
    }
    if (process.env.INTERNAL_AUDIT && (await exports.isCiauUser(username))) {
        return;
    }
    const dataCodes = await connectors_1.cache.get_as_obj(username + '#' + functionCode + '#');
    args.where = (_a = args.where) !== null && _a !== void 0 ? _a : {};
    if (!dataCodes) {
        args.where['id'] = { in: ['DUMMY_ID'] };
    }
    else if (dataCodes[0] !== 'ALL') {
        for (const dataField of dataFields) {
            args.where[dataField] = { in: dataCodes };
        }
    }
};
const makeContext = (params) => {
    let token = {};
    let isInternal = false;
    if (!params.req) {
        return {
            token,
            isInternal: false,
            tenantId: getTenant(token),
            rawToken: '',
            isAuthorized,
            dataCodes,
            applyDataCodes,
            isSuperAdmin: exports.isSuperAdmin,
            isConcurrentApprover: exports.isConcurrentApprover,
            isCiauUser: exports.isCiauUser,
        };
    }
    if (params.req.headers['x-internal'] === 'y') {
        isInternal = true;
    }
    token = getToken(params);
    return {
        token,
        isInternal: isInternal,
        tenantId: getTenant(token),
        rawToken: params.req.headers.authorization,
        isAuthorized,
        dataCodes,
        applyDataCodes,
        isSuperAdmin: exports.isSuperAdmin,
        isConcurrentApprover: exports.isConcurrentApprover,
        isCiauUser: exports.isCiauUser,
    };
};
exports.makeContext = makeContext;
const isSuperAdmin = async (ctx) => {
    return await getUserRole(SUPER_ADMIN, ctx.token.username);
};
exports.isSuperAdmin = isSuperAdmin;
const isConcurrentApprover = async (ctx) => {
    return await getUserRole(CONCURRENT_APPROVER, ctx.token.username);
};
exports.isConcurrentApprover = isConcurrentApprover;
const isCiauUser = async (userName) => {
    return await getUserRole(GLOBAL_CIAU, userName);
};
exports.isCiauUser = isCiauUser;
async function getUserRole(roleId, userName) {
    let isValid = false;
    if (!userName) {
        console.log('While validating the superadmin role, Unable to find the details of user name', userName);
        throw Error('Unable to find the details of user token');
    }
    const key = 'ADM_VALIDATION#' + userName + roleId;
    const userRole = await connectors_1.cache.get(key);
    if (userRole) {
        return !isValid;
    }
    const query = `
    query userRoles($role_id: String!, $user_name: String!, $is_active: Boolean!) {
        userRoles(
          where: {
            role_id: { equals: $role_id }
            user: { user_name: { equals: $user_name } }
            is_active: { equals: $is_active }
          }
        ) {
          id
          manager_id
          user_id
        }
      }
    `;
    const result = await connectors_1.api.request(process.env.SYS_ADM_API_URL || '', query, { role_id: roleId, user_name: userName, is_active: true }, '', {
        userId: 'SYSTEM',
        username: 'SYSTEM',
        email: '',
        tenantId: 'TEN1',
    });
    if (result && result.userRoles && result.userRoles.length > 0) {
        await connectors_1.cache.set(key, result.userRoles);
        isValid = true;
    }
    return isValid;
}
//# sourceMappingURL=context.js.map