"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.compute = exports.auth = void 0;
const connectors_1 = require("../connectors");
const auth = (publicRoutes) => {
    return async (root, args, ctx, info, next) => {
        // don't process for non root resolvers
        if (info.path.prev) {
            return await next(root, args, ctx, info);
        }
        // authenticate
        // const resolver = `${_config.parentTypeConfig.name}.${_config.fieldConfig.name}`
        const resolver = `${info.parentType}.${info.path.key}`;
        if (!publicRoutes.includes(resolver)) {
            if (!(ctx.token && ctx.token.userId)) {
                connectors_1.error.throwAuthError('system.auth.unauthorized');
            }
        }
        const result = await next(root, args, ctx, info);
        return result;
    };
};
exports.auth = auth;
const compute = () => {
    return async (root, args, ctx, info, next) => {
        // const resolver = `${_config.parentTypeConfig.name}.${_config.fieldConfig.name}`
        // const resolver = `${info.parentType}.${info.path.key}`
        if (args && args.data) {
            args.data.tenant_id = ctx.tenantId;
            args.data['version_user'] = ctx.token.username;
        }
        return await next(root, args, ctx, info);
    };
};
exports.compute = compute;
//# sourceMappingURL=schema.js.map