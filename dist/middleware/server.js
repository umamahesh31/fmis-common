"use strict";
// import { error } from '@tectoro/connectors'
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorHandler = exports.sanitiser = void 0;
const sanitiser = (req, res, next) => {
    res.removeHeader('Transfer-Encoding');
    res.removeHeader('X-Powered-By');
    next();
};
exports.sanitiser = sanitiser;
// global error handler
const errorHandler = async (req, res, next) => {
    try {
        await next();
    }
    catch (error) {
        console.log('Internal error');
        console.error(error);
        error.throwServiceError('system.global.error');
    }
};
exports.errorHandler = errorHandler;
//# sourceMappingURL=server.js.map