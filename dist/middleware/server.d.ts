export declare const sanitiser: (req: any, res: any, next: any) => void;
export declare const errorHandler: (req: any, res: any, next: any) => Promise<void>;
