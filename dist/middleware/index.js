"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.middleware = void 0;
const tslib_1 = require("tslib");
// module
// export * from './nexus'
const context_1 = require("./context");
const server_middleware = tslib_1.__importStar(require("./server"));
const schema_middleware = tslib_1.__importStar(require("./schema"));
exports.middleware = {
    makeContext: context_1.makeContext,
    server_middleware,
    schema_middleware,
};
//# sourceMappingURL=index.js.map