import { core } from 'nexus';
declare global {
    export type Arg<T extends string> = core.GetGen3<'argTypes', 'Mutation', T>;
    export type MutationArg<T extends string> = Arg<T>;
    export type QueryArg<T extends string> = core.GetGen3<'argTypes', 'Query', T>;
}
import * as server_middleware from './server';
import * as schema_middleware from './schema';
export declare const middleware: {
    makeContext: (params: any) => any;
    server_middleware: typeof server_middleware;
    schema_middleware: typeof schema_middleware;
};
