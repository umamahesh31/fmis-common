export declare const makeContext: (params: any) => any;
export declare const isSuperAdmin: (ctx: any) => Promise<any>;
export declare const isConcurrentApprover: (ctx: any) => Promise<any>;
export declare const isCiauUser: (userName: string) => Promise<any>;
