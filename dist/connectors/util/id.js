"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.id = void 0;
const tslib_1 = require("tslib");
const cuid_1 = tslib_1.__importDefault(require("cuid"));
exports.id = {
    generate: () => {
        return cuid_1.default();
    },
};
//# sourceMappingURL=id.js.map