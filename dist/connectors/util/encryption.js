"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.encrypter = exports.CONFIG = void 0;
const crypto_1 = require("crypto");
exports.CONFIG = {
    key: process.env.ENCRYPTION_KEY || 'd6F3Efeq',
    algorithm: 'aes-256-ctr',
};
exports.encrypter = {
    encrypt: (input) => {
        const cipher = crypto_1.createCipher(exports.CONFIG.algorithm, exports.CONFIG.key);
        let crypted = cipher.update(input, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    },
    decrypt: (encryptedText) => {
        const decipher = crypto_1.createDecipher(exports.CONFIG.algorithm, exports.CONFIG.key);
        let dec = decipher.update(encryptedText, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    },
};
//# sourceMappingURL=encryption.js.map