export declare const commons: {
    isValidCuid: (ref: any) => boolean;
    activateRecordsWrtEffectiveDate: (ctx: any, tableName: string) => Promise<void>;
};
