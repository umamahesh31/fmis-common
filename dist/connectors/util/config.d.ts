import { DotenvConfigOptions } from 'dotenv';
export declare const loadEnv: (options?: DotenvConfigOptions | undefined) => void;
export declare function getValidationConfig(module: string): any;
