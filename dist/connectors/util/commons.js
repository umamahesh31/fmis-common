"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.commons = void 0;
const tslib_1 = require("tslib");
const cuid_1 = tslib_1.__importDefault(require("cuid"));
exports.commons = {
    isValidCuid: (ref) => {
        if (ref === '' || ref === null || ref === undefined) {
            return true;
        }
        if (cuid_1.default.isCuid(ref)) {
            return true;
        }
        return false;
    },
    activateRecordsWrtEffectiveDate: async (ctx, tableName) => {
        let effectiveFrom = new Date();
        let effectiveTo = new Date();
        const status = 'Approved';
        effectiveFrom.setUTCHours(0, 0, 0, 0);
        effectiveTo.setUTCHours(23, 59, 59, 59);
        let query = `select id,ref from ${tableName} where effective_from >= '${effectiveFrom.toISOString()}' and effective_from <= '${effectiveTo.toISOString()}' and is_effective = FALSE and is_active = TRUE
        and status = '${status}'`;
        const rows = await ctx.db.$queryRaw(query);
        if (!rows || rows.length == 0) {
            console.log('no records found to activate');
            return;
        }
        const refsToUpdate = ['DUMMY'];
        const idsToUpdate = ['DUMMY'];
        for (const row of rows) {
            refsToUpdate.push(row.ref);
            idsToUpdate.push(row.id);
        }
        const updateQry1 = `UPDATE ${tableName}  SET is_latest=FALSE, is_effective=FALSE WHERE ref IN (${enrichArrayForSqlQry(refsToUpdate)})`;
        const updateQry2 = `UPDATE ${tableName}  SET is_effective =is_active,is_latest=TRUE WHERE id IN (${enrichArrayForSqlQry(idsToUpdate)})`;
        await ctx.db.$executeRaw(updateQry1);
        await ctx.db.$executeRaw(updateQry2);
    },
};
const enrichArrayForSqlQry = (arr) => {
    return JSON.stringify(arr)
        .replace(/[\[\]']+/g, '')
        .replace(/["]/g, `'`);
};
//# sourceMappingURL=commons.js.map