"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getValidationConfig = exports.loadEnv = void 0;
const tslib_1 = require("tslib");
const dotenv_1 = require("dotenv");
const fs = tslib_1.__importStar(require("fs"));
const path = tslib_1.__importStar(require("path"));
const connectors_1 = require("../../connectors");
let rawData;
const loadEnv = (options) => {
    dotenv_1.config(options);
};
exports.loadEnv = loadEnv;
function getValidationConfig(module) {
    const filePath = path.join('/app/config', 'validations.json');
    if (!rawData) {
        rawData = fs.readFileSync(filePath);
    }
    const obj = JSON.parse(rawData);
    if (!obj || !obj[module]) {
        console.log('no data found in validation found against: ', module);
        connectors_1.error.throwUserError('system.global.error');
    }
    return obj[module];
}
exports.getValidationConfig = getValidationConfig;
//# sourceMappingURL=config.js.map