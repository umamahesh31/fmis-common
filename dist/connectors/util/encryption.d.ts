export declare const CONFIG: {
    key: string;
    algorithm: string;
};
export declare const encrypter: {
    encrypt: (input: string) => any;
    decrypt: (encryptedText: string) => any;
};
