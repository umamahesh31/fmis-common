import { getValidationConfig } from './config';
export declare const util: {
    CONFIG: {
        key: string;
        algorithm: string;
    };
    encrypter: {
        encrypt: (input: string) => any;
        decrypt: (encryptedText: string) => any;
    };
    id: {
        generate: () => string;
    };
    loadEnv: (options?: import("dotenv").DotenvConfigOptions | undefined) => void;
    getValidationConfig: typeof getValidationConfig;
    commons: {
        isValidCuid: (ref: any) => boolean;
        activateRecordsWrtEffectiveDate: (ctx: any, tableName: string) => Promise<void>;
    };
};
