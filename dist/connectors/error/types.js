"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceError = exports.ForbiddenError = exports.AuthenticationError = exports.UserInputError = void 0;
// Client Errors
class UserInputError extends Error {
    constructor() {
        super(...arguments);
        this.status = 400;
    }
}
exports.UserInputError = UserInputError;
class AuthenticationError extends Error {
    constructor() {
        super(...arguments);
        this.status = 401;
    }
}
exports.AuthenticationError = AuthenticationError;
class ForbiddenError extends Error {
    constructor() {
        super(...arguments);
        this.status = 403;
    }
}
exports.ForbiddenError = ForbiddenError;
// Service Errors
class ServiceError extends Error {
    constructor() {
        super(...arguments);
        this.status = 500;
    }
}
exports.ServiceError = ServiceError;
//# sourceMappingURL=types.js.map