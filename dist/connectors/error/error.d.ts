export declare const prepareMessage: (code: string, ...params: any[]) => any;
export declare const throwAuthError: (code: string, ...params: any[]) => void;
export declare const throwForbiddenError: (code: string, ...params: any[]) => void;
export declare const throwUserError: (code: string, ...params: any[]) => void;
export declare const throwServiceError: (code: string, ...params: any[]) => void;
