"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.throwServiceError = exports.throwUserError = exports.throwForbiddenError = exports.throwAuthError = exports.prepareMessage = void 0;
const dotenv_1 = require("dotenv");
const types_1 = require("./types");
const props = dotenv_1.config({
    path: 'messages.properties',
}).parsed || {};
const format = function (template, ...params) {
    return template.replace(/{(\d+)}/g, function (match, number) {
        return typeof params[number] !== 'undefined' ? params[number] : match;
    });
};
const prepareMessage = (code, ...params) => {
    const template = props[code];
    if (!template) {
        throw new types_1.ServiceError('Failed with unknown error code ' + code);
    }
    return format(template, ...params);
};
exports.prepareMessage = prepareMessage;
const throwAuthError = (code, ...params) => {
    const message = exports.prepareMessage(code, ...params);
    throw new types_1.AuthenticationError(message);
};
exports.throwAuthError = throwAuthError;
const throwForbiddenError = (code, ...params) => {
    const message = exports.prepareMessage(code, ...params);
    throw new types_1.ForbiddenError(message);
};
exports.throwForbiddenError = throwForbiddenError;
const throwUserError = (code, ...params) => {
    const message = exports.prepareMessage(code, ...params);
    throw new types_1.UserInputError(message);
};
exports.throwUserError = throwUserError;
const throwServiceError = (code, ...params) => {
    const message = exports.prepareMessage(code, ...params);
    throw new types_1.ServiceError(message);
};
exports.throwServiceError = throwServiceError;
//# sourceMappingURL=error.js.map