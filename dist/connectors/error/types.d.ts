export declare class UserInputError extends Error {
    status: number;
}
export declare class AuthenticationError extends Error {
    status: number;
}
export declare class ForbiddenError extends Error {
    status: number;
}
export declare class ServiceError extends Error {
    status: number;
}
