import { Message, Stan } from 'node-nats-streaming';
import { MUTATION_EVENT, WF_STATUS_EVENT, AUDIT_EVENT, UPDATE_EVENT_HANDLER } from './types';
export declare function getVersion(): string;
export declare const CONFIG: {
    topics: {
        mutation: string;
        wfstatus: string;
        audit: string;
        bill: string;
        update_event: string;
    };
};
export declare const doconnect: (clientId: string) => Promise<Stan>;
export declare const subscribe: <T extends unknown>(clientId: string, groupId: string, topic: string, next: (msg: {
    mesg: Message;
    data: T;
}) => void) => Promise<void>;
export declare const publish: (topic: string, input: any) => Promise<any>;
/** *********************************************** */
/** *********************************************** */
export declare const subscribeMutation: (clientId: string, groupId: string, next: (msg: {
    mesg: Message;
    data: MUTATION_EVENT;
}) => void) => Promise<void>;
export declare const subscribeAudit: (clientId: string, groupId: string, next: (msg: {
    mesg: Message;
    data: AUDIT_EVENT;
}) => void) => Promise<void>;
export declare const subscribeWfUpdate: (clientId: string, groupId: string, next: (msg: {
    mesg: Message;
    data: WF_STATUS_EVENT;
}) => void) => Promise<void>;
export declare const subscribeUpdateHandler: (clientId: string, groupId: string, next: (msg: {
    mesg: Message;
    data: UPDATE_EVENT_HANDLER;
}) => void) => Promise<void>;
export declare const publishMutation: (input: MUTATION_EVENT) => Promise<any>;
export declare const publishAudit: (input: AUDIT_EVENT) => Promise<any>;
export declare const publishUpdateEvent: (input: UPDATE_EVENT_HANDLER) => Promise<any>;
export declare const publishWfStatus: (input: WF_STATUS_EVENT) => Promise<any>;
