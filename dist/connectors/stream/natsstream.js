"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publishWfStatus = exports.publishUpdateEvent = exports.publishAudit = exports.publishMutation = exports.subscribeUpdateHandler = exports.subscribeWfUpdate = exports.subscribeAudit = exports.subscribeMutation = exports.publish = exports.subscribe = exports.doconnect = exports.CONFIG = exports.getVersion = void 0;
const node_nats_streaming_1 = require("node-nats-streaming");
const schema_1 = require("./schema");
function getVersion() {
    // <application_name>.<version>.<topic_name>
    return (process.env.APP_NAME || 'APP') + '_' + (process.env.VERSION || 'V10') + '_';
}
exports.getVersion = getVersion;
exports.CONFIG = {
    topics: {
        mutation: 'mutation',
        wfstatus: 'wfstatus',
        audit: 'audit',
        bill: 'bill',
        update_event: 'update_event',
    },
};
const doconnect = async (clientId) => {
    return new Promise(function (resolve, reject) {
        clientId = getVersion() + clientId;
        const conn = node_nats_streaming_1.connect(process.env.NATS_STREAM_CLUSTER || 'test-cluster', clientId, {
            url: process.env.NATS_STREAM_URL || 'nats://localhost:4223',
            reconnect: true,
        });
        conn.on('connect', async () => {
            resolve(conn);
        });
        conn.on('error', async (err) => {
            console.log('error occurred in connect:', err);
            reject(err);
        });
    });
};
exports.doconnect = doconnect;
const subscribe = async (clientId, groupId, topic, next) => {
    const client = await exports.doconnect(clientId);
    topic = getVersion() + topic;
    const options = client.subscriptionOptions().setStartAt(node_nats_streaming_1.StartPosition.NEW_ONLY).setDurableName(topic).setManualAckMode(true);
    // .setAckWait(1 * 60 * 1000) //1 min
    let subscription;
    if (groupId !== '') {
        subscription = client.subscribe(topic, groupId, options);
        console.log(`connected to subject ${topic} on queue ${groupId} as ${clientId}`);
    }
    else {
        subscription = client.subscribe(topic, options);
        console.log(`connected to subject ${topic} as ${clientId}`);
    }
    subscription.on('message', async (mesg) => {
        let body = {};
        try {
            body = JSON.parse(mesg.getData());
        }
        catch (e) {
            console.log(e);
        }
        next({
            mesg,
            data: body,
        });
    });
    client.on('close', () => {
        console.log('closing subscription');
        subscription.unsubscribe();
    });
    process.on('exit', function (code) {
        subscription.unsubscribe();
        return console.log(`About to exit with code ${code}, Unsubscribing Subscripton with clientID: ${clientId}`);
    });
};
exports.subscribe = subscribe;
// TODO: maintain persistent connection
const publish = async (topic, input) => {
    console.log('publishing the message');
    topic = getVersion() + topic;
    const clientId = process.env.NATS_STREAM_CLIENTID || 'testpub';
    const conn = await exports.doconnect(clientId);
    const resp = conn.publish(topic, JSON.stringify(input));
    conn.close();
    return resp;
};
exports.publish = publish;
/** *********************************************** */
// Wrapper Functions
/** *********************************************** */
const subscribeMutation = async (clientId, groupId, next) => {
    return exports.subscribe(clientId, groupId, exports.CONFIG.topics.mutation, next);
};
exports.subscribeMutation = subscribeMutation;
const subscribeAudit = async (clientId, groupId, next) => {
    return exports.subscribe(clientId, groupId, exports.CONFIG.topics.audit, next);
};
exports.subscribeAudit = subscribeAudit;
const subscribeWfUpdate = async (clientId, groupId, next) => {
    return exports.subscribe(clientId, groupId, exports.CONFIG.topics.wfstatus, next);
};
exports.subscribeWfUpdate = subscribeWfUpdate;
const subscribeUpdateHandler = async (clientId, groupId, next) => {
    return exports.subscribe(clientId, groupId, exports.CONFIG.topics.update_event, next);
};
exports.subscribeUpdateHandler = subscribeUpdateHandler;
const publishMutation = async (input) => {
    const errors = schema_1.schema.validate(input, schema_1.schema.types.entity.entity_mutation);
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors);
        return errors;
    }
    await exports.publish(exports.CONFIG.topics.mutation, input);
    return [];
};
exports.publishMutation = publishMutation;
const publishAudit = async (input) => {
    const errors = schema_1.schema.validate(input, schema_1.schema.types.audit);
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors);
        return errors;
    }
    await exports.publish(exports.CONFIG.topics.audit, input);
    return [];
};
exports.publishAudit = publishAudit;
/*
  To publish the update event to handle the generic things over all services
  in input object has to send the event type, based on the event_type at each
  api end needs to be handled and in 'info' object we can send the required
  information.
*/
const publishUpdateEvent = async (input) => {
    const errors = schema_1.schema.validate(input, schema_1.schema.types.update_event);
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors);
        return errors;
    }
    await exports.publish(exports.CONFIG.topics.update_event, input);
    return [];
};
exports.publishUpdateEvent = publishUpdateEvent;
const publishWfStatus = async (input) => {
    const errors = schema_1.schema.validate(input, schema_1.schema.types.workflow.wf_status_event);
    if (errors.length > 0) {
        console.log('Failed to publish as validation failed:', errors);
        return errors;
    }
    await exports.publish(exports.CONFIG.topics.wfstatus, input);
    return [];
};
exports.publishWfStatus = publishWfStatus;
//# sourceMappingURL=natsstream.js.map