import { Schema } from 'jsonschema';
export declare const schema: {
    types: {
        entity: {
            entity_record: Schema;
            entity_mutation: Schema;
        };
        workflow: {
            wf_details: Schema;
            wf_status_event: Schema;
        };
        audit: Schema;
        update_event: Schema;
    };
    validate: (input: any, schema: Schema) => any;
};
