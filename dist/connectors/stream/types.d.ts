/** *********************************************** */
/** *********************************************** */
export declare type ENTITY = {
    id: string;
    version_no: number;
    version_user: string;
    version_date: string;
    [key: string]: any;
};
export declare type MUTATION_EVENT = {
    type: string;
    action: 'create' | 'update' | 'delete';
    record: ENTITY;
    extra?: WF_DETAILS;
};
/** *********************************************** */
/** *********************************************** */
export declare type AUDIT_EVENT = {
    source_id: string;
    version_no: number;
    prev_version_no?: number;
};
/** *********************************************** */
/** *********************************************** */
export declare type WF_DETAILS = {
    wf_conf_id: string;
};
export declare type WF_STATUS_EVENT = {
    function: string;
    sub_function: string;
    source_id: string;
    source_ref: string;
    source_ref_updated: string;
    status: string;
    version_date: Date;
};
/** *********************************************** */
/** *********************************************** */
export declare type UPDATE_EVENT_HANDLER = {
    function: string;
    source_id: string;
    source_ref: string;
    status: string;
    event_type: string;
    info: any;
};
export declare const MUTATION_ACTIONS: string[];
export declare type WF_PHASE = 'make' | 'review' | 'approve';
export declare type WF_APPROVE_STATUS = 'ACCEPTED' | 'REJECTED' | '';
