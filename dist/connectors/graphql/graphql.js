"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.request = void 0;
const graphql_request_1 = require("graphql-request");
const cross_fetch_1 = require("cross-fetch");
const request = (apiURL, query, variables, rawToken, token) => {
    const client = new graphql_request_1.GraphQLClient(apiURL);
    global.Headers = global.Headers || cross_fetch_1.Headers;
    if (token) {
        client.setHeader('x-email', token.email);
        client.setHeader('x-tenantid', token.tenantId);
        client.setHeader('x-userid', token.userId);
        client.setHeader('x-username', token.username);
    }
    client.setHeader('x-internal', 'y');
    client.setHeader('authorization', rawToken);
    return client.request(query, variables);
};
exports.request = request;
//# sourceMappingURL=graphql.js.map