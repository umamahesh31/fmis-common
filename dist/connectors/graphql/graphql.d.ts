import { AuthToken } from '../auth';
export declare const request: (apiURL: string, query: string, variables: any, rawToken: string, token: AuthToken) => any;
