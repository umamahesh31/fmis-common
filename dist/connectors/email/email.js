"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.send = void 0;
const Email = require('email-templates');
const nodemailer = require('nodemailer');
const path = require('path');
let mailTemplate;
const getMailTemplate = () => {
    const transport = nodemailer.createTransport({
        host: process.env.SMTP_HOST || '',
        port: new Number(process.env.SMTP_PORT || '465').valueOf(),
        secure: new Boolean(process.env.SMTP_SECURE || false).valueOf(),
        auth: {
            user: process.env.SMTP_USER || '',
            pass: process.env.SMTP_PASSWORD || '',
        },
        debug: true,
    });
    return new Email({
        message: {
            from: process.env.SMTP_FROM || '',
        },
        send: true,
        transport: transport,
        preview: false,
    });
};
const send = (template, data, to) => {
    if (!mailTemplate)
        mailTemplate = getMailTemplate();
    mailTemplate
        .send({
        template: path.join('/app/config', 'emails', template),
        message: {
            to: to,
            attachments: data.attachments,
        },
        locals: data,
    })
        .then((res) => {
        // console.log('Response for sending the email', res.originalMessage)
    })
        .catch((err) => console.log(err));
};
exports.send = send;
//# sourceMappingURL=email.js.map