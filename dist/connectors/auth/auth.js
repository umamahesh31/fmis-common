"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hashCompare = exports.hash = exports.refresh = exports.deencrypt = exports.encrypt = void 0;
const tslib_1 = require("tslib");
const jsonwebtoken_1 = require("jsonwebtoken");
const bcrypt = tslib_1.__importStar(require("bcryptjs"));
const encrypt = (userId, username, email, tenantId) => {
    const expires = { expiresIn: 60 * 60 };
    return jsonwebtoken_1.sign({
        userId,
        username,
        email,
        tenantId,
    }, process.env.JWT_SECRET || '', expires);
};
exports.encrypt = encrypt;
const deencrypt = (token) => {
    return jsonwebtoken_1.verify(token, process.env.JWT_SECRET || '');
};
exports.deencrypt = deencrypt;
const refresh = (token) => {
    const data = jsonwebtoken_1.decode(token);
    return exports.encrypt(data.userId, data.username, data.email, data.tenantId);
};
exports.refresh = refresh;
const hash = (password) => {
    return bcrypt.hashSync(password, 10);
};
exports.hash = hash;
const hashCompare = (password1, password2) => {
    return bcrypt.compare(password1, password2);
};
exports.hashCompare = hashCompare;
//# sourceMappingURL=auth.js.map