export declare const encrypt: (userId: string, username: string, email: string, tenantId: string) => any;
export declare const deencrypt: (token: string) => any;
export declare const refresh: (token: string) => any;
export declare const hash: (password: string) => any;
export declare const hashCompare: (password1: string, password2: string) => any;
