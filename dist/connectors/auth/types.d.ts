export declare type AuthToken = {
    userId: string;
    username: string;
    email: string;
    tenantId: string;
};
