"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.type = exports.renamenx = exports.randomkey = exports.pttl = exports.pexpireat = exports.pexpire = exports.persist = exports.move = exports.keys = exports.expireat = exports.del = exports.expire = exports.ttl = exports.hvals = exports.hstrlen = exports.hsetnx = exports.hset = exports.hmset = exports.hmget = exports.hlen = exports.hkeys = exports.hincrbyfloat = exports.hincrby = exports.hgetall = exports.hget = exports.hexists = exports.hdel = exports.get_ttl = exports.set_ttl = exports.includeVersionKey = exports.get_keys = exports.flush_all = exports.clear_all = exports.clear = exports.exists = exports.mset = exports.set = exports.get_as_obj = exports.get = exports.cacheConnect = void 0;
const tedis_1 = require("tedis");
const natsstream_1 = require("../stream/natsstream");
let tedis1;
// TODO: check on how to auto reconnect
const connect = async () => {
    if (tedis1) {
        return tedis1;
    }
    else {
        // const tedispool = new TedisPool({
        //     host: process.env.REDIS_HOST || 'localhost',
        //     port: parseInt(process.env.REDIS_PORT || '') || 6379,
        // })
        // const tedis = await tedispool.getTedis()
        // tedispool.putTedis(tedis)
        // tedis1 = tedis
        tedis1 = new tedis_1.Tedis({
            host: process.env.REDIS_HOST || 'localhost',
            port: parseInt(process.env.REDIS_PORT || '') || 6379,
        });
        let tds;
        tedis1.on('connect', () => {
            console.log('connect');
        });
        tedis1.on('timeout', () => {
            console.log('timeout');
        });
        tedis1.on('error', (err) => {
            console.log(err);
            tedis1 = tds;
        });
        tedis1.on('close', (had_error) => {
            console.log('close with err: ', had_error);
            tedis1 = tds;
        });
        return tedis1;
    }
};
const cacheConnect = async () => {
    return await connect();
};
exports.cacheConnect = cacheConnect;
const get = async (key) => {
    return await (await connect()).get(exports.includeVersionKey(key));
};
exports.get = get;
const get_as_obj = async (key) => {
    const value = await (await connect()).get(exports.includeVersionKey(key));
    return value && typeof value === 'string' ? JSON.parse(value) : value;
};
exports.get_as_obj = get_as_obj;
const set = async (key, value, expiryMins = 0) => {
    const tds = await connect();
    key = exports.includeVersionKey(key);
    await tds.set(key, typeof value === 'object' ? JSON.stringify(value) : value);
    if (expiryMins > 0) {
        await tds.expire(key, expiryMins * 60);
    }
};
exports.set = set;
const mset = async (info, versionKeyIncluded) => {
    const tds = await connect();
    const newMapObj = {};
    if (versionKeyIncluded) {
        for (const property in info) {
            newMapObj[exports.includeVersionKey(property)] = info[property];
        }
        await tds.mset(newMapObj);
    }
    else {
        await tds.mset(info);
    }
};
exports.mset = mset;
const exists = async (key) => {
    const tds = await connect();
    return (await tds.exists(exports.includeVersionKey(key))) === 1;
};
exports.exists = exists;
const clear = async (key) => {
    const tds = await connect();
    await tds.del(exports.includeVersionKey(key));
};
exports.clear = clear;
const clear_all = async (keyPattern) => {
    const tds = await connect();
    const keys = await tds.keys(keyPattern);
    let keysToDelete = [];
    const batchSize = process.env.REDIS_BATCH_SIZE || 10000;
    for (const key of keys) {
        keysToDelete.push(key);
        if (keysToDelete.length === batchSize) {
            await tds.del('', ...keysToDelete);
            keysToDelete = [];
        }
    }
    if (keysToDelete.length > 0) {
        await tds.del('', ...keysToDelete);
    }
};
exports.clear_all = clear_all;
const flush_all = async () => {
    const tds = await connect();
    await tds.command('FLUSHALL');
};
exports.flush_all = flush_all;
const get_keys = async (keyPattern) => {
    const tds = await connect();
    return await tds.keys(keyPattern);
};
exports.get_keys = get_keys;
const includeVersionKey = (key) => {
    const versionKey = natsstream_1.getVersion().toUpperCase();
    key = versionKey + key;
    return key;
};
exports.includeVersionKey = includeVersionKey;
const set_ttl = async (key, expireTime) => {
    const tds = await connect();
    await tds.set(exports.includeVersionKey(key), expireTime);
};
exports.set_ttl = set_ttl;
const get_ttl = async (key) => {
    const tds = await connect();
    return await tds.ttl(exports.includeVersionKey(key));
};
exports.get_ttl = get_ttl;
const hdel = async (key, field, ...fields) => {
    return await (await connect()).hdel(exports.includeVersionKey(key), field, ...fields);
};
exports.hdel = hdel;
const hexists = async (key, field) => {
    return await (await connect()).hexists(exports.includeVersionKey(key), field);
};
exports.hexists = hexists;
const hget = async (key, value) => {
    return await (await connect()).hget(exports.includeVersionKey(key), value);
};
exports.hget = hget;
const hgetall = async (key) => {
    return await (await connect()).hgetall(exports.includeVersionKey(key));
};
exports.hgetall = hgetall;
const hincrby = async (key, field, increment) => {
    return await (await connect()).hincrby(exports.includeVersionKey(key), field, increment);
};
exports.hincrby = hincrby;
const hincrbyfloat = async (key, field, increment) => {
    return await (await connect()).hincrbyfloat(exports.includeVersionKey(key), field, increment);
};
exports.hincrbyfloat = hincrbyfloat;
const hkeys = async (key) => {
    return await (await connect()).hkeys(exports.includeVersionKey(key));
};
exports.hkeys = hkeys;
const hlen = async (key) => {
    return await (await connect()).hlen(exports.includeVersionKey(key));
};
exports.hlen = hlen;
const hmget = async (key, field, ...fields) => {
    return await (await connect()).hmget(exports.includeVersionKey(key), field, ...fields);
};
exports.hmget = hmget;
const hmset = async (key, hash) => {
    return await (await connect()).hmset(exports.includeVersionKey(key), hash);
};
exports.hmset = hmset;
const hset = async (key, field, value) => {
    return await (await connect()).hset(exports.includeVersionKey(key), field, value);
};
exports.hset = hset;
const hsetnx = async (key, field, value) => {
    return await (await connect()).hsetnx(exports.includeVersionKey(key), field, value);
};
exports.hsetnx = hsetnx;
const hstrlen = async (key, field) => {
    return await (await connect()).hstrlen(exports.includeVersionKey(key), field);
};
exports.hstrlen = hstrlen;
const hvals = async (key) => {
    return await (await connect()).hvals(exports.includeVersionKey(key));
};
exports.hvals = hvals;
const ttl = async (key) => {
    return await (await connect()).ttl(exports.includeVersionKey(key));
};
exports.ttl = ttl;
const expire = async (key, seconds) => {
    return await (await connect()).expire(exports.includeVersionKey(key), seconds);
};
exports.expire = expire;
const del = async (key, ...keys) => {
    return await (await connect()).del(exports.includeVersionKey(key), ...keys);
};
exports.del = del;
const expireat = async (key, timestamp) => {
    return await (await connect()).expireat(exports.includeVersionKey(key), timestamp);
};
exports.expireat = expireat;
const keys = async (pattern) => {
    return await (await connect()).keys(pattern);
};
exports.keys = keys;
const move = async (key, db) => {
    return await (await connect()).move(exports.includeVersionKey(key), db);
};
exports.move = move;
const persist = async (key) => {
    return await (await connect()).persist(exports.includeVersionKey(key));
};
exports.persist = persist;
const pexpire = async (key, milliseconds) => {
    return await (await connect()).pexpire(exports.includeVersionKey(key), milliseconds);
};
exports.pexpire = pexpire;
const pexpireat = async (key, millisecondsTimestamp) => {
    return await (await connect()).pexpireat(exports.includeVersionKey(key), millisecondsTimestamp);
};
exports.pexpireat = pexpireat;
const pttl = async (key) => {
    return await (await connect()).pttl(exports.includeVersionKey(key));
};
exports.pttl = pttl;
const randomkey = async () => {
    return await (await connect()).randomkey();
};
exports.randomkey = randomkey;
const renamenx = async (key, newKey) => {
    return await (await connect()).renamenx(exports.includeVersionKey(key), newKey);
};
exports.renamenx = renamenx;
const type = async (key) => {
    return await (await connect()).type(key);
};
exports.type = type;
//# sourceMappingURL=cache.js.map