"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const api = tslib_1.__importStar(require("../graphql/index"));
const testReq = async () => {
    const query = `
    query user($user_name:String!){
        user(where:{user_name:$user_name}){
          user_name
          is_active
          employee{
              deputation_office
          }
        }
      }`;
    const result = await api.api.request('https://fmis.dev.tectoro.com/api/sysadmin/graphql', query, { user_name: 'samba_001' }, '', {
        email: 'samba.y@tectoro.com',
        tenantId: 'TEN1',
        userId: 'ckdwux80d0254m6nbkqfi40yf',
        username: 'User326',
    });
    console.log('result', result);
};
testReq();
//# sourceMappingURL=testAPI.js.map