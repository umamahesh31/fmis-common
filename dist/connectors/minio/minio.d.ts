import { Client } from 'minio';
export declare function getClient(): Client;
export declare const upload: (stream: any, uploadName: string) => Promise<any>;
export declare const download: (filename: string) => Promise<any>;
export declare const remove: (filename: string) => Promise<any>;
export declare function getBucket(): string;
