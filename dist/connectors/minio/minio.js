"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBucket = exports.remove = exports.download = exports.upload = exports.getClient = void 0;
const minio_1 = require("minio");
let minioClient;
function getClient() {
    if (minioClient != null) {
        return minioClient;
    }
    minioClient = new minio_1.Client({
        endPoint: process.env.MINIO_ENDPOINT || 'localhost',
        port: new Number(process.env.MINIO_PORT || '9000').valueOf(),
        useSSL: false,
        accessKey: process.env.MINIO_ACCESS_KEY || 'minio',
        secretKey: process.env.MINIO_SECRET_KEY || 'minio123',
    });
    return minioClient;
}
exports.getClient = getClient;
const upload = async (stream, uploadName) => {
    await getClient().putObject(getBucket(), uploadName, stream);
    const stats = await minioClient.statObject(getBucket(), uploadName);
    return stats;
};
exports.upload = upload;
const download = async (filename) => {
    return await getClient().getObject(getBucket(), filename);
};
exports.download = download;
const remove = async (filename) => {
    return await getClient().removeObject(getBucket(), filename);
};
exports.remove = remove;
function getBucket() {
    return process.env.MINIO_BUCKET || 'bucket';
}
exports.getBucket = getBucket;
//# sourceMappingURL=minio.js.map