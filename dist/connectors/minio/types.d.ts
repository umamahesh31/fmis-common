export interface FILE_METADATA {
    [key: string]: any;
}
export declare type FILE_STATS = {
    size: number;
    etag: string;
    lastModified: Date;
    metaData: FILE_METADATA;
};
